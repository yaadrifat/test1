package com.velos.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Handler
 */
public class ProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProxyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		callService(request, response, "GET");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		callService(request, response, "POST");
	}
	
	
	private void callService(HttpServletRequest request, HttpServletResponse response, String callType)
	throws ServletException, IOException
	{
		response.setContentType("application/json");
		URL url = new URL(request.getParameter("url"));		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(callType);
		conn.setRequestProperty("Content-Type", "application/json");	
		
	
		conn.setRequestProperty ("Authorization", 	request.getHeader("Authentication"));
		String input = request.getParameter("input"); 
 
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
 
		PrintWriter writer = response.getWriter();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
//			throw new RuntimeException("Failed : HTTP error code : "
//				+ conn.getResponseCode());
			writer.println("Error Occured" + conn.getResponseCode()); 
			writer.flush();
		}else
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
	 
			String output;
			
			while ((output = br.readLine()) != null) {
				writer.println(output);
			}			
			writer.flush();			
		}
 
	}

}
