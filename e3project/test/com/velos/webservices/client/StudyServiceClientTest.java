package com.velos.webservices.client;

import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class StudyServiceClientTest {
	private StudyServiceClient client = null;
	private static final String domain = "http://66.237.42.88";
	private static final String userId = "ihuang";
	private static final String password = "velos123";
	private static final String STUDY_NUMBER = "studyNumber";
	private JSONObject jObj = null;
	
	@Before
	public void setUp() {
		client = new StudyServiceClient(domain, userId, password);
		jObj = new JSONObject();
		try {
			jObj.put(STUDY_NUMBER, "NCT00344994 SWITCH");
		} catch(Exception e) {}
	}
	
	@Test
	public void testGetStudy() {
		System.out.println(client.getStudy(jObj.toString()));
		assertTrue(true);
	}
	
	@Test
	public void testGetStudyWriter() {
		PrintWriter writer = new PrintWriter(System.out);
		client.getStudy(jObj.toString(), writer);
		writer.flush();
		assertTrue(true);
	}
}
