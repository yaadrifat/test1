package com.velos.webservices.client;

import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class PatientDemographicsServiceClientTest {
	private PatientDemographicsServiceClient client = null;
	private static final String domain = "http://66.237.42.104";
	private static final String userId = "jxiong";
	private static final String password = "velos123";
	private static final String PATIENT_ID = "patientId";
	private static final String ORGANIZATION_ID = "organizationId";
	private static final String SITE_NAME = "siteName";
	
	private JSONObject jObj = null;
	@Before
	public void setUp() {
		client = new PatientDemographicsServiceClient(domain, userId, password);
		jObj = new JSONObject();
		try {
			jObj.put(PATIENT_ID, "453134");
			JSONObject organizationIdentifier = new JSONObject();
			organizationIdentifier.put(SITE_NAME, "Velos");
			jObj.put(ORGANIZATION_ID, organizationIdentifier);
		} catch(Exception e) {}
	}
	
	@Test
	public void testGetPatientDemographics() {
		PrintWriter writer = new PrintWriter(System.out);
		client.getPatientDemographics(jObj.toString(), writer);
		writer.flush();
		int i = 1;
		assertTrue(i == 1);
	}
	
	@Test
	public void testGetPatientFacilities() {
		PrintWriter writer = new PrintWriter(System.out);
		client.getPatientFacilities(jObj.toString(), writer);
		writer.flush();
		int i = 1;
		assertTrue(i == 1);
	}

	@Test
	public void testGetPatientDetails() {
		PrintWriter writer = new PrintWriter(System.out);
		client.getPatientDetails(jObj.toString(), writer);
		writer.flush();
		int i = 1;
		assertTrue(i == 1);
	}
}
