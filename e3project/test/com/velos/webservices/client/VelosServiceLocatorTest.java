package com.velos.webservices.client;

import static org.junit.Assert.*;

import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Test;

public class VelosServiceLocatorTest {
	String domain = null;
	@Before
	public void setUp() {
		domain = "http://66.237.42.88";
	}
	
	@Test
	public void testGetRegisteredServiceMethod() {
		assertTrue("getStudy".equals(VelosServiceLocator.getRegisteredServiceMethod("studyservice", "getStudy")));
	}

	@Test
	public void testGetStudyService() {
		try {
			assertTrue(VelosServiceLocator.getStudyService(domain) != null);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPrintHelloFalse() {
		try {
			assertFalse("hello".equals(VelosServiceLocator.getStudyService(domain)));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
}
