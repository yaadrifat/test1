package com.velos.webservices.client;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class VelosServiceBundleTest {
	@Test
	public void testGetProperty() {
		String allowedHostList = VelosServiceBundle.getProperty("allowed.host.list");
		String[] allowedHosts = allowedHostList.split(",");
		
		boolean localhostFound = false;
		for (int iX=0; iX<allowedHosts.length; iX++) {
			if (allowedHosts[iX] == null) { continue; }
			allowedHosts[iX] = allowedHosts[iX].trim();
			if (allowedHosts[iX].equals("localhost")) {
				localhostFound = true;
			}
		}
		assertTrue(localhostFound);
	}

}
