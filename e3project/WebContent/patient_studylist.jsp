
<div class="accordion-group">
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#pat_studyListAccordion" href="#collapseOne"> Study
			List </a>
	</div>
	<div id="collapseOne" class="accordion-body collapse in">
		<div class="accordion-inner">
			<div class="container">
				<div id="pat_studyListInsertTable"></div>
			</div>
		</div>
	</div>
</div>
