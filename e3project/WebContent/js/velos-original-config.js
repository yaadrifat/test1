/** 
 * Date: Apr-30-2013
 * 
 * List of all configurable JavaScript data.
 * 
 * This list contains the configurable switches with the default value for each switch.
 * To override the default value, create a file called 'velos-custom-config.js' and
 * then copy the line or block from 'velos-original-config.js' to 'velos-custom-config.js' file
 * and customize the values there. DO NOT CUSTOMIZE ANYTHING IN THIS FILE (velos-original-config.js).
 * 
 * Note that this is a site-wide (not an account-wide) configuration. 
 * All these data will be applied to all accounts in a shared/ASP environment.
 * 
 * Developers: Organize all data by functional categories and make sure to add sufficient documentation.
 */

// --- Start of configurable items
/**
 *  MY_WS_DOMAIN_PATH defines the domain that hosts the web services. It is used in the Ajax call.
 *  The convention for the value is [protocol]+[IP or domain]+[port] where the port defaults to 80 if empty.
 *  For example, the value of 'http+127.0.0.1+8080' gets converted to http://127.0.0.1:8080 in the WS call.
 *  The value 'http+127.0.0.1' gets converted to http://127.0.0.1:80.
 */
var MY_WS_DOMAIN_PATH = 'http+127.0.0.1';

// Add more configurable items here...
