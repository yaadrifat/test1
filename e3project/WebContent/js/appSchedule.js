//below is configuring datatables

//First, some functions we will use later...
function restoreRow ( oTable, nRow )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);

	for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
		oTable.fnUpdate( aData[i], nRow, i, false );
	}

	oTable.fnDraw();
}


function editRow ( oTable, nRow )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);
	jqTds[0].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[0]+'" disabled/>';
	jqTds[1].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[1]+'" disabled/>';
	jqTds[2].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[2]+'" disabled/>';
	jqTds[3].innerHTML = '<select id ="event_status" type="text" class="inputDatatables" value="'+aData[3]+'"><option value="done">Done</option><option value="done_osh">Done at OSH</option><option value="not_done">Not Done</option><option value="not_required">Not Required</option><option value="to_be">To be Rescheduled</option><option value="billing_reviewed">Billing Reviewed</option><option value="past_date">Past Scheduled Date</option></select>';
	jqTds[4].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[4]+'" disabled/>';
	jqTds[5].innerHTML = '<select id ="event_status" type="text" class="inputDatatables" value="'+aData[5]+'"><option value="done">Done</option><option value="done_osh">Done at OSH</option><option value="not_done">Not Done</option><option value="not_required">Not Required</option><option value="to_be">To be Rescheduled</option><option value="billing_reviewed">Billing Reviewed</option><option value="past_date">Past Scheduled Date</option></select>';
	jqTds[6].innerHTML = '<input type="text" class="inputDatatables datepicker" value="'+aData[6]+'">';
	jqTds[7].innerHTML = '<a class="edit btn btn-small btn-kanchan " href="">Save</a>';
	jqTds[8].innerHTML = '<a class="delete btn btn-small btn-lima" href=""><i class="icon-trash"></i></a>';
	$('.inputDatatables').css('width', '125px');

}

function saveRow ( oTable, nRow )
{
	var jqInputs = $('input', nRow);
	var jqSelects = $('select', nRow);
	var statuses = [{value:"done",display:"Done"}, {value:"done_osh",display:"Done at OSH"}, {value:"not_done",display:"Not done"},{value:"not_required", display:"Not Required"},{value:"to_be", display:"To be Rescheduled"},{value:"billing_reviewed",display:"Billing Reviewed"},{value:"past_date",display:"Past Scheduled Date"}];
	/*for(var i =0; i<statuses.length; i++) {
				alert("status"+ statuses[i].display);
				} */
	//oTable.fnUpdate( jqInputs[0].value, nRow, 0, false ); - if checkbox, show here
	oTable.fnUpdate( jqInputs[0].value, nRow, 0, false );
	oTable.fnUpdate( jqInputs[1].value, nRow, 1, false );
	oTable.fnUpdate( jqInputs[2].value, nRow, 2, false );
	oTable.fnUpdate( statuses[jqSelects[0].selectedIndex ? jqSelects[0].selectedIndex : 0].display, nRow, 3, false );
	oTable.fnUpdate( jqInputs[3].value, nRow, 4, false );
	oTable.fnUpdate( statuses[jqSelects[1].selectedIndex ? jqSelects[1].selectedIndex : 0].display, nRow, 5, false );
	oTable.fnUpdate( jqInputs[4].value, nRow, 6, false );
	oTable.fnUpdate( '<a class="edit btn btn-small btn-sea " href=""><i class="icon-pencil"></i></a>', nRow, 7, false );
	oTable.fnUpdate( '<a class="delete btn btn-small btn-lima" href=""><i class="icon-trash"></i></a>', nRow, 8, false );
	oTable.fnDraw();
}

$(document).ready(function() {
	$('#scheduleOfEventsInsertTable').html( '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="scheduleOfEventsTable"></table>' );
	var oTable = $('#scheduleOfEventsTable').dataTable( {

		"aaData": [
		           //Reduced data set - next step is to feed this variable with incoming/parsed .json or xml
		           [ "Baseline", "10002", "TRF8201", "Not Done", "Evaluation Visit 1", "Done", "Mar 4, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>" ],
		           [ "Evaluation", "01004", "NCT2001","Done","Initial Visit", "Done", "Mar 4, 2013",  "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>" ],
		           [ "Hemoglobin Count", "03067", "JKR0021","Not Required","Follow-up Visit 1", "Done", "Mar 4, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Anesthesia", "15234", "NCT2984","Not Required","Dental Cleaning 1", "Done", "Mar 15, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Injection", "82234", "TRF8822" ,"Done", "Dental Cleaning 1", "Done", "Mar 15, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Hemoglobin Count", "12367", "SGF0021","Done","Dental Cleaning 1", "Done", "Mar 15, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "X-ray", "14123", "PRW5501","Done" , "Evaluation Visit", "Not Required", "Apr 2, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Baseline", "21334", "NCT29484","Done", "Initial Visit", "Not Done", "Apr 2, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Evaluation", "53256", "NCT29484","Not Done", "Dental Cleaning 2", "Not Required", "Apr 2, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Anesthesia", "84927", "NCT00321" ,"Done", "Dental Cleaning 2", "Done", "Apr 2, 2013","<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Injection", "98762", "KE290001" ,"Done", "Dental Cleaning 2", "Not Done", "Apr 2, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Hemoglobin Count", "12343", "TRF8823" ,"Done", "Lab Testing", "Done", "Apr 14, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "X-ray", "10098", "JFRK003" ,"Done", "Dental Cleaning 3", "Not Done", "Apr 14, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"],
		           [ "Injection", "34876", "NCT2004" ,"Done", "Dental Cleaning 3", "Not Required", "Apr 21, 2013", "<a class=\"edit btn btn-small btn-sea \" href=\"\"><i class=\"icon-pencil\"></i></a>",  "<a class=\"delete btn btn-small btn-lima\" href=\"\"><i class=\"icon-trash\"></i></a>"]
		           ],
		           //column names will be handled a bit different...for now like this, later should be based on the incoming json
		           "aoColumns": [
		                         { "sTitle": "Event Name" },
		                         { "sTitle": "Patient ID" },
		                         { "sTitle": "Study ID" },
		                         { "sTitle": "Event Status" },
		                         { "sTitle": "Visit Name", "sClass": "center" },
		                         { "sTitle": "Visit Status"},
		                         { "sTitle": "Scheduled Date" },
		                         { "sTitle": "Edit"}, 
		                         { "sTitle": "Delete"},
		                         ],

		                         "sDom": "R<'span2'C><'row'<'span3'l><'span6'T><'span2'f>r>t<'row'<'span6'i><'span6'p>>",
		                         "sPaginationType": "bootstrap",
		                         "oLanguage": {
		                        	 "sLengthMenu": "_MENU_ records per page"
		                         },
		                         "aaSorting":[[0, "asc"]],
		                         "iDisplayLength": 10,
		                         "oTableTools": {
		                        	 //"sSwfPath": "http://localhost/copy_csv_xls_pdf.swf",
		                        	 "sRowSelect": "multi",
		                        	 //"sSelectedClass": "row_selected",
		                        	 "aButtons": [
		                        	              "copy",
		                        	              "print",
		                        	              {
		                        	            	  "sExtends":    "collection",
		                        	            	  "sButtonText": 'Export <span class="caret" />',
		                        	            	  "aButtons":    [ "csv", "xls", "pdf" ]
		                        	              }, 
		                        	              {
		                        	            	  "sExtends":    "collection",
		                        	            	  "sButtonText": 'Select <span class="caret" />',
		                        	            	  "aButtons":    [ "select_all", "select_none"]
		                        	              },
		                        	              ]
		                         }, 


		                         /* "bProcessing": false,
			        "bServerSide": true,
			        "sAjaxSource": "scripts/server_processing.php",
			        "fnDrawCallback": function () {
			            $('#example tbody td').editable( '../examples_support/editable_ajax.php', {
			                "callback": function( sValue, y ) {
			                    /* Redraw the table from the new data on the server 
			                    oTable.fnDraw();
			                },
			                "height": "14px"
			            } );
			        }*/
	} );



	$('button').addClass('btn DTTT_button_collection');

	$(document).on('click', '.ColVis_MasterButton', function(){
		$('.ColVis_collection').find('button').addClass('btn btn-india').css({'width':'150px'}).css({'margin-buttom':'0.9px'});
		$(".ColVis_radio").find('input').css({'float':'left'}).css({'margin':'6px 7px 0px 0px'});
		$(".ColVis_title").css({'float':'left'});
	});

	// add a button "New" to add a new row
	var $appendButtonNew = $('<a id="ToolTables_scheduleOfEventsTable_7" class="btn"><a/>'),
	$appendNameNew = $('<span>Add Event</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonNew);
	$('#ToolTables_scheduleOfEventsTable_7').append($appendNameNew);

	//add a button "Bulk Edit" to edit selected items
	var $appendButtonEdit = $('<a href="#" id="ToolTables_scheduleOfEventsTable_8" class="btn"><a/>'),
	$appendNameEdit = $('<span>Bulk Edit</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonEdit);
	$('#ToolTables_scheduleOfEventsTable_8').append($appendNameEdit);


	// add a calendar
	var $appendButtonCalendar = $('<a href="calendar.html" id="ToolTables_scheduleOfEventsTable_9" class="btn"><a/>'),
	$appendNameCalendar = $('<span>Calendar View</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonCalendar);
	$('#ToolTables_scheduleOfEventsTable_9').append($appendNameCalendar);


	var nEditing = null;

	//add new raw
	$('#ToolTables_scheduleOfEventsTable_7').on('click', function() {
		//e.preventDefault();

		// if I am using checkboxes
		/*var aiNew = oTable.fnAddData( [ '<input type="checkbox" class="checkbox" name="checkbox">','', '', '', '', '', 
						'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );*/

		var aiNew = oTable.fnAddData( ['', '', '', '', '', '', '',
		                               '<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );

		var nRow = oTable.fnGetNodes( aiNew[0] );
		editRow( oTable, nRow );
		nEditing = nRow;
	});


	//delete a new row
	$('#scheduleOfEventsTable a.delete').on('click', function (e) {
		e.preventDefault();

		var nRow = $(this).parents('tr')[0];
		oTable.fnDeleteRow( nRow );
		$('#myModal').modal('show');
	} );


	// edit a new raw within the table
	$(document).on('click', '#scheduleOfEventsTable a.edit', function (e) {
		e.preventDefault();
		/* Get the row as a parent of the link that was clicked on */
		var nRow = $(this).parents('tr')[0];

		if ( nEditing !== null && nEditing != nRow ) {
			/* Currently editing - but not this row - restore the old before continuing to edit mode */
			restoreRow( oTable, nEditing );
			editRow( oTable, nRow );
			nEditing = nRow;
		}


		else if ( nEditing == nRow && this.innerHTML == "Save" ) {
			/* Editing this row and want to save it */
			saveRow( oTable, nEditing );
			nEditing = null;
			$('#myModal').modal('show');
		}

		else {
			/* No edit in progress - let's start one */
			editRow( oTable, nRow );
			nEditing = nRow;

		}
	} );



}); //comes with datatables


$(document).ready(function(){
	// cycling added by yevheniy and daisuke
	$('#ToolTables_scheduleOfEventsTable_8').on('click', function() {


		//Create Edit DataSet
		//e.preventDefault();
		var oTT = TableTools.fnGetInstance( 'scheduleOfEventsTable');
		var aData = oTT.fnGetSelectedData();
		var eData = aData;
		var nowRowNum = 0;
		var DataSet = [aData, nowRowNum, eData];
		//alert(DataSet);
		$("#scheduleOfEventsCycleOne").data("DataSet", DataSet);
		//alert($('#scheduleOfEventsCycleOne').data('DataSet'));


		/*alert(aData[0]);
							alert(aData[1]);
							alert(aData.length);*/

		// add the events (columns data) on top of the modal window. Remove if pressed delete. Remove data from eData array..
		//...so that when do not cycle through the deleted data
		/*var i;
							for (i=0; i<eData.length; i++) {

								var $fieldsetForm = ('<fieldset data-role="fieldcontain" id="removeFields'+i+'" class="fieldset"></fieldset>'),
									$label = $('<label for="name'+i+'" class="ui-hidden-accessible"></label>'),
			              			$inputlabel = $('<input type="text" name="name_label[]" id="name'+i+'">'),
						    		//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
						    		$inputbutton = $('<a href="#" id="btnDel'+i+'" class="btnDel" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="c" data-inline="true">Delete</a>'); 

								$('#formFields').prepend($fieldsetForm);
								$('#removeFields'+i).append($label, $inputlabel, $inputbutton);
								$('#name'+i).val(eData[i][0]);

								//any of the below solutions will work
								//$("#formFields").on('click', '.btnDel', function(e) {
									//$(this).closest("fieldset").remove();
								$('#btnDel'+i).live('click', function() { // deprecated starting jQuery 1.9 http://api.jquery.com/live/
									$(this).closest("fieldset").remove();
									var currentID = $(this).attr('id');
									var removePosition = currentID[currentID.length-1];
									eData.splice(removePosition, 1);

									//alert(aData[i]);

									//for (k=0; k<eData.length; k++) {
									//	alert (eData[k]);
									//}
								});
							}	*/	

		$('#scheduleOFEventsModalBody').prepend('<div id="scheduleOfEventsSlimScroll"></div>')				
		$('#scheduleOfEventsSlimScroll').slimScroll({
			color: '#00f',
			size: '10px',
			height: '80px',
			alwaysVisible: true,
			position: 'right', 
			railVisible: true,
			distance: '5px',
		});




		var i;
		for (i=0; i<eData.length; i++) {

			var $scheduleOfEventsInputAppend = ('<div id="removeFields'+i+'" class="input-append"></div>'),
			$scheduleOfEventsInput = $('<input type="text" class="span2" id="name'+i+'">'),
			//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
			$inputbutton = $('<button id="btnDel'+i+'" class="btn" type="button"><i class="icon-remove-circle"></i></button>'); 

			$('#scheduleOfEventsSlimScroll').append($scheduleOfEventsInputAppend);
			$('#removeFields'+i).append($scheduleOfEventsInput, $inputbutton);
			$('#name'+i).val(eData[i][0]);

			//any of the below solutions will work
			//$("#formFields").on('click', '.btnDel', function(e) {
			//$(this).closest("fieldset").remove();
			$(document).on('click', '#btnDel'+i, function() { // "live" is deprecated starting jQuery 1.9 http://api.jquery.com/live/
				$(this).closest("div").remove();
				var currentID = $(this).attr('id');
				var removePosition = currentID[currentID.length-1];
				eData.splice(removePosition, 1);

				//alert(aData[i]);

				/*for (k=0; k<eData.length; k++) {
										alert (eData[k]);
									}*/
			});


		}

		$("#scheduleOfEventsCycleOne").modal();
		//erase cache on initial close
		$(document).on('click', '#scheduleOfEventsClose', function(){
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});


	});

	$('#scheduleOfEventsApplyToAll').on('click', function() {
		$("#scheduleOfEventsConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="scheduleOfEventsApplyToAllPreviewButton">Preview</a>')
		$("#cycle1").remove();
		$(this).remove();
	});

	// cycle through celected raws
	$("#cycle1").on('click', function() {

		// when i first cycle, remove those fields I added above. 
		//$(".fieldset").remove();


		// added hack to fix jQuery Mobile's internal bug - otherwise page (specifically conent of the dialog and data)..
		//..is added twice upon click event. If you find a better way, please, apply
		/*var beforeTimeStamp = $("#scheduleOfEventsCycleOne").data("ts");
								//alert(beforeTimeStamp);
								if (beforeTimeStamp != undefined){
								    if (e.timeStamp - beforeTimeStamp < 100) {
									      return;
									}
								}
								//alert(e.timeStamp);
								$("#scheduleOfEventsCycleOne").data("ts", e.timeStamp);*/

		//alert(DataSet[1]);
		//e.preventDefault();

		$("#scheduleOfEventsApplyToAll").remove();
		$('.slimScrollDiv').remove();

		//alert( $('#edit_d1').data('DataSet'));
		var DataSet   = $('#scheduleOfEventsCycleOne').data('DataSet');
		//alert(DataSet[1]);
		var eData     = DataSet[2];
		var nowRowNum = DataSet[1];
		var aData   = DataSet[0];
		var maxRowNum = aData.length;


		/*alert(nowRowNum);
								alert(maxRowNum);*/
		if (nowRowNum == maxRowNum-1) {


			//all of this to just add preview button upon the last cycle. Will simplify with time
			$("#cycle1").remove();
			/*

										$('#footerdialog1').append('<li class="ui-block-c" id="addpreviewli"></li>');
										$("#addpreviewli").append('<a href="#" id="addpreviewa" data-role="button" data-icon="bars" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="span" data-theme="a" class="ui-btn ui-btn-inline ui-btn-icon-left ui-btn-up-a" data-iconpos="left" data-inline="true"></a>'); 
										$("#addpreviewa").append('<span class="ui-btn-inner" id="addpreviewspan1"></span>');
										$("#addpreviewspan1").append('<span class="ui-btn-text" id="addpreviewspan2">Preview</span>');
										$("#addpreviewspan1").append('<span class="ui-icon ui-icon-bars ui-icon-shadow" id="addpreviewspan3">&nbsp;</span>');*/
			$("#scheduleOfEventsConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="scheduleOfEventsPreviewButton">Preview</a>');
		}

		//saving to eData	
		$('#text-c1').val(eData[nowRowNum][0]);
		$('#text-c2').val(eData[nowRowNum][1]);
		$('#text-c3').val(eData[nowRowNum][2]);
		$('#text-c4').val(eData[nowRowNum][3]);
		$('#text-c5').val(eData[nowRowNum][4]);
		$('#text-c6').val(eData[nowRowNum][5]);
		$('#text-c7').val(eData[nowRowNum][6]);

		$('#scheduleOfEventsModalPagination').html("<p>"+(nowRowNum+1)+ " of "+eData.length+" </p>")


		//increment nowRow
		DataSet[1]++;

		$('#scheduleOfEventsCycleOne').data("DataSet", DataSet);


		//erase cache on cycle close
		$(document).on('click', '#scheduleOfEventsClose', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#scheduleOfEventsCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});


	// add reason for change...more work will be needed as every time i leave the field a next textearea will be added...
	$('.reasonForChange').change(function(){
		$(this).after('<textarea rows="2" placeholder="Reason for change"></textarea>');
	});

	// confirm
	$(document).on('click', '.preview', function() {

		$('#scheduleOfEventsCycleOne').modal('hide');
		//e.preventDefault();

		// same as above
		/*var beforeTimeStamp = $("#edit_d1").data("ts");
							if (beforeTimeStamp != undefined){
								if (e.timeStamp - beforeTimeStamp < 100) {
								return;
								}
							}
							//alert(e.timeStamp);
							$("#cheduleOfEventsCycleOne").data("ts", e.timeStamp);*/



		/*view confirm*/
		var DataSet   = $('#scheduleOfEventsCycleOne').data('DataSet');
		var eData     = DataSet[0];
		for (var i = 0; i < eData.length; i++) {

			// insert preview data afte cycling is done
			$('<th>'+eData[i][0]+'</th>').insertAfter('#headerInsertAfter');
			$('<td>'+eData[i][0]+'</td>').insertAfter('#c1InsertAfter');
			$('<td>'+eData[i][1]+'</td>').insertAfter('#c2InsertAfter');
			$('<td>'+eData[i][2]+'</td>').insertAfter('#c3InsertAfter');
			$('<td>'+eData[i][3]+'</td>').insertAfter('#c4InsertAfter');
			$('<td>'+eData[i][4]+'</td>').insertAfter('#c5InsertAfter');
			$('<td>'+eData[i][5]+'</td>').insertAfter('#c6InsertAfter');
			$('<td>'+eData[i][6]+'</td>').insertAfter('#c7InsertAfter');


		}

		$("#scheduleOfEventsConfirm").modal();


		/*$(document).on('click', '#scheduleOfEventsClose', function(){
								eData=[]; 
								aData=[]; 
								DataSet = [[], 0, []];
								$('.input-append').empty();
								$('#scheduleOfEventsCycleOne').modal('hide');

							});*/

		//erase cache on closing preview
		$(document).on('click', '.closeEraseCache', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#scheduleOfEventsCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});

	$('#scheduleOfEventsTable_filter').find('input').attr('placeholder', 'Search List');

	//$(".second").pageslide({ direction: "left", modal: true });


	// handling submit buttons
	$(document).on('click', '#submitButton', function() {

		// submit validation somewhere here
		$('#myModal').modal('hide');

	});

	$(document).on('click', '#submitPreviewButton', function() {

		// submit validation somewhere here
		$('#scheduleOfEventsConfirm').modal('hide');

	});


	$('.ColVis_MasterButton').addClass('btn btn-primary');

	/*$(document).on('click', '#slidingNotesLaningView', function() {
					alert('hi');
					$.pageslide({direction:'left', href:'slidingnotes.html'});
			});*/

} ); //document.ready close


$(document).ready(function(){
	$('[data-slidepanel]').slidepanel({
		orientation: 'left',
		mode: 'push'
	});
	$('body').css({
		backgroundColor: '#FFFFFF',
		color: '#333333',
		fontFamily:' "Helvetica Neue",Helvetica,Arial,sans-serif',
		fontSize: '14px',
		lineHeight: '20px',
		margin: 0
	});
});

// ajax call 


// As a reminder: I will need to write ajax calls for each web service connection: meaning that ...
//..to extract patient demographics info based on the input ({"PatientIdentifier":{"PK":187}}) I will..
//..need one ajax. The success function will parse and insert the data into the form elemnts (canvas).
// Then to show tabular data for shcedule of events, i will need another ajax call. Within..
//..this ajax call,  i will need to define a function that will convert my json input into arrary of arrays,..
//..parse the data and then assign it to aaData variable...

//Below is the parsing and inserting of the patient demographics data
/*function make_base_auth(user, pass) {
						  var tok = user + ':' + pass;
						  //var hash = Base64.encode(tok);
						  return "Basic " ;
						}*/

// this is how we call actual webservices...I will start using it once the parser is ready
/*$(document).ready(function(){
	        $("input#gsearch").bind("change",autoFill);
	          });

			   function autoFill(){

			   // alert("hii");

			   var auth = make_base_auth('kchetal','velos123');
	           var url = 'http://66.237.42.105/webservices/rest/patientdemographicservice/getpatientdemographics';
	           var input = '{"PatientIdentifier":{"PK":187}}'; //for dynamically requesting data, I assume we will have to point this to the "search" input field...
	           // e.i... input = $('input#gsearch').val(); - something like this???
	           var param='url='+url+"&input="+input;

				 $.ajax({

		                   cache: true,
		           url: "/E3/Handler",
		           data: param,
		           type: "POST",
		           dataType:'json',
		           beforeSend : function(req) {
		               req.setRequestHeader('Authorization', auth);
		           },
		           success:function(resp){
		                               console.log(resp.toString());
		                               alert(resp.PatientDemographics.address1.toString());
		                               // write my own parser for form (canvas) data

		                                 //alert(resp.PatientDemographics.city.toString());
		                                 //alert(resp.CodeTypes.type[1].codeType.toString())
		                                 //$.each(resp.CodeTypes.type, function(index, value) {
		                               	 //console.log(value.codeType.toString() + ":" + value.description.toString()); 
		                               });

		                       },
		           error: function(XMLHttpRequest, textStatus, errorThrown) {
		           		alert('Error: ' + errorThrown);
		           }

		        });			 
			}*/



// below is handling canvas (form) data, coming from patient demographics service (or any other service)..
//..for conveniecne I am using local file (not local server file)
$(document).ready(function(){

	// alert("hii");

	/*var auth = make_base_auth('kchetal','velos123');
	           var url = 'http://66.237.42.105/webservices/rest/patientdemographicservice/getpatientdemographics';
	           var input = '{"PatientIdentifier":{"PK":187}}'; //for dynamically requesting data, I assume we will have to point this to the "search" input field...
	           // e.i... input = $('input#gsearch').val(); - something like this???
	           var param='url='+url+"&input="+input;*/

	$.ajax({

		cache: true,
		url: "patientdemographics.json",
		//data: param,
		type: "POST",
		dataType:'json',
		/*beforeSend : function(req) {
		               req.setRequestHeader('Authorization', auth);
		           },*/
		success:function(resp){
			// alert(resp.toString());
			//alert(resp.PatientDemographics.bloodGroup.toString());
			$('#maritalStatus').val(resp.PatientDemographics.maritalStatus.toString());
			$('#bloodGroup').val(resp.PatientDemographics.bloodGroup.toString());
			$('#ethnicity').val(resp.PatientDemographics.ethnicity.toString());
			$('#race').val(resp.PatientDemographics.race.toString());
			$('#ssn').val(resp.PatientDemographics.SSNNot.toString());
			$('#address1').val(resp.PatientDemographics.address1.toString());
			$('#city').val(resp.PatientDemographics.city.toString());
			$('#county').val(resp.PatientDemographics.county.toString());
			$('#zipCode').val(resp.PatientDemographics.zipCode.toString());
			$('#country').val(resp.PatientDemographics.country.toString());
			$('#timezone').val(resp.PatientDemographics.timeZone.description.toString());
			$('#organization').val(resp.PatientDemographics.organization.siteName.toString());
			$('#facilityID').val(resp.PatientDemographics.organization.ID.toString());
			$('#registrationDate').val(resp.PatientDemographics.registrationDate.toString());
			$('#homePhone').val(resp.PatientDemographics.homePhone.toString());

			// write my own parser for form (canvas) data

			//alert(resp.PatientDemographics.city.toString());
			//alert(resp.CodeTypes.type[1].codeType.toString())
			//$.each(resp.CodeTypes.type, function(index, value) {
			//console.log(value.codeType.toString() + ":" + value.description.toString()); 
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert('Error: ' + errorThrown);
		}

	});	


	$(".datepicker").datepicker();
	$(".datepicker").css('z-index', '60000');

	// sorting of accordians

	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();

});

//Below is handling table data, coming from shcedule of events service (or any other service)
/*$(document).ready(function() {

			   // alert("hii");
					//not used locally
					   var auth = make_base_auth('kchetal','velos123');
			           var url = 'http://66.237.42.105/webservices/rest/patientdemographicservice/getpatientdemographics';
			           var input = '{"PatientIdentifier":{"PK":187}}'; //I will point it to pull the data from the entire shedule of events service(for example)
			           var param='url='+url+"&input="+input;

				 $.ajax({

		          //cache: true,
		          // url: "/E3/Handler",
		           cache: true,
		           type: 'POST',
		           //url: "patientdemographics.json",
		           url: "patientschedule.json",
		           //data: param,
		           //data: {'pd': 'Hello'},

		           dataType:'json',
		           			// not used when local
					           beforeSend : function(req) {
					               req.setRequestHeader('Authorization', auth);
					           },
		           success:function(data){


		           						// function(resp){ - for the service

				 						// write my own parser for table data; first convert json to array of arrays

				 						var aaData =[];
										console.log(data);
										getArrayFromJSON(data, aaData);
										console.log(aaData);
							},
		           error: function(XMLHttpRequest, textStatus, errorThrown) {
		           		alert('Error: ' + errorThrown);
		           }

		        });			 

			}); */

//convert json into arrayofarrays - for now it only works for json of jsons, however, patient event schedule comes with json of arrays of json..blahblah
function getArrayFromJSON(obj, retArray) {
	for (var key in obj) {
		if(isObject(obj[key]) == true) {
			var aData = [];
			getArrayFromJSON(obj[key], aData);
			retArray.push(aData);
		}
		else {
			retArray.push(obj[key]);
		}
	}
}
function isObject(obj) {
	return obj instanceof Object && Object.getPrototypeOf(obj) === Object.prototype;
}

/*$(document).ready(function(){
				$(".datepicker").datepicker();
			});
 */
