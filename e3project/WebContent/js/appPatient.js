//below is configuring datatables

//First, some functions we will use later...
/*function restoreRow ( oTable, nRow )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);

	for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
		oTable.fnUpdate( aData[i], nRow, i, false );
	}

	oTable.fnDraw();
}*/

/*function editRow ( oTable, nRow )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);
	jqTds[0].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[0]+'">';
	jqTds[1].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[1]+'">';
	jqTds[2].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[2]+'">';
	jqTds[3].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[3]+'">';
	jqTds[4].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[4]+'">';
	jqTds[5].innerHTML = '<input type="text" class="inputDatatables" value="'+aData[5]+'">';
	jqTds[6].innerHTML = '<input type="text" class="inputDatatables"value="'+aData[6]+'">';
	jqTds[7].innerHTML = '<a class="edit btn btn-small btn-kanchan " href="">Save</a>';
	jqTds[8].innerHTML = '<a class="delete btn btn-small btn-lima" href=""><i class="icon-trash"></i></a>';
	$('.inputDatatables').css('width', '125px');
}*/

/*function saveRow ( oTable, nRow )
{
	var jqInputs = $('input', nRow);
	//oTable.fnUpdate( jqInputs[0].value, nRow, 0, false ); - if checkbox, show here
	oTable.fnUpdate( jqInputs[0].value, nRow, 0, false );
	oTable.fnUpdate( jqInputs[1].value, nRow, 1, false );
	oTable.fnUpdate( jqInputs[2].value, nRow, 2, false );
	oTable.fnUpdate( jqInputs[3].value, nRow, 3, false );
	oTable.fnUpdate( jqInputs[4].value, nRow, 4, false );
	oTable.fnUpdate( jqInputs[5].value, nRow, 5, false );
	oTable.fnUpdate( jqInputs[6].value, nRow, 6, false );
	oTable.fnUpdate( '<a class="edit btn btn-small btn-sea " href=""><i class="icon-pencil"></i></a>', nRow, 7, false );
	oTable.fnUpdate( '<a class="delete btn btn-small btn-lima" href=""><i class="icon-trash"></i></a>', nRow, 8, false );
	oTable.fnDraw();
}
 */

$('document').ready(function(){
	$("#selectall").click(function () {
		$('.td').attr('checked', this.checked);
	});
	$(".td").click(function(){
		if($(".td").length == $(".td:checked").length) {
			$("#selectall").attr("checked", "checked");
		} else {
			$("#selectall").removeAttr("checked");
		}
	});
});

function formatDate(o) {
	return o.year+ '-' +(o.month<10?'0':'')+o.month +'-'+(o.day<10?'0':'')+o.day;

}
/*function setDate(o) {
	return o.year+ '-' +(o.month<10?'0':'')+o.month +'-'+(o.day<10?'0':'')+o.day;

}*/
//----------------------------------------------Study List--------------------------------------------------------------------------
$(document).ready(function() {
	$('#pat_studyListInsertTable').html( '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="pat_studyListTable"></table>' );
	var oTable = $('#pat_studyListTable').dataTable( {

		"aaData": [
		           //Reduced data set - next step is to feed this variable with incoming/parsed .json or xml
		           [ "","TRF8201", "Skin Disease Study", "Enrolled", "Trial Status: Active Enrolling", "Tim Thomas"],
		           [ "","NCT2001", "Brain Research", "Enrolled","Budget: Approved by PI", "Paulina Jones" ],
		           [ "","JKR0021", "Lung Cancer Study","Completed","Trial Status: Active Enrolling","Jil Walters"],
		           [ "","NCT2984", "Phase 3 Drug Trial","Completed","Trial Status: Terminated", "Kelly Nanatani"]
		           ],
		           //column names will be handled a bit different...for now like this, later should be based on the incoming json
		           aoColumnDefs  : [
		                            {
		                            	aTargets: [0],    // Column number which needs to be modified
		                            	fnRender: function (o, v) {   // o, v contains the object and value for the column
		                            		return '<input type="checkbox" id="selectall" name="selectall" />';
		                            	},
		                            	sClass: 'tableCell'    // Optional - class to be applied to this table cell
		                            }],
		                            "aoColumns": [
		                                          {"sTitle": "<input type='checkbox'></input>","mDataProp": null, 
		                                        	  "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>", 
		                                        	  "bSortable": false},
		                                        	  { "sTitle": "Study Number" },
		                                        	  { "sTitle": "Study Title" },
		                                        	  { "sTitle": "Patient Study Status", "sClass": "center" },
		                                        	  { "sTitle": "Study Status" },
		                                        	  { "sTitle": "Principal Investigator"},
		                                        	  /*{ "sTitle": "Start Date" },
		                         { "sTitle": "End Date" },*/
		                                        	  /*{ "sTitle": "Edit"}, */
		                                        	  ],


		                                        	  "sDom": "R<'span2'C><'row'<'span3'l><'span4'T><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
		                                        	  "sPaginationType": "bootstrap",
		                                        	  "oLanguage": {
		                                        		  "sLengthMenu": "_MENU_ records"
		                                        	  },
		                                        	  "aaSorting":[[0, "asc"]],
		                                        	  "iDisplayLength": 5,
		                                        	  "oTableTools": {
		                                        		  //"sSwfPath": "http://localhost/copy_csv_xls_pdf.swf",
		                                        		  "sRowSelect": "multi",
		                                        		  //"sSelectedClass": "row_selected",
		                                        		  "aButtons": [
		                                        		               /* "copy", */
		                                        		               /* "print",*/
		                                        		               /* {
		                        	            	  "sExtends":    "collection",
		                        	            	  "sButtonText": 'Export <span class="caret" />',
		                        	            	  "aButtons":    [ "csv", "xls", "pdf" ]
		                        	              }, */
		                                        		               /* {
		                        	            	  "sExtends":    "collection",
		                        	            	  "sButtonText": 'Select <span class="caret" />',
		                        	            	  "aButtons":    [ "select_all", "select_none"]
		                        	              },*/
		                                        		               ]
		                                        	  }, 


		                                        	  /* "bProcessing": false,
			        "bServerSide": true,
			        "sAjaxSource": "scripts/server_processing.php",
			        "fnDrawCallback": function () {
			            $('#example tbody td').editable( '../examples_support/editable_ajax.php', {
			                "callback": function( sValue, y ) {
			                    /* Redraw the table from the new data on the server 
			                    oTable.fnDraw();
			                },
			                "height": "14px"
			            } );
			        }*/
	} );



	$('button').addClass('btn DTTT_button_collection');

	$(document).on('click', '.ColVis_MasterButton', function(){
		$('.ColVis_collection').find('button').addClass('btn btn-india').css({'width':'150px'}).css({'margin-buttom':'0.9px'});
		$(".ColVis_radio").find('input').css({'float':'left'}).css({'margin':'6px 7px 0px 0px'});
		$(".ColVis_title").css({'float':'left'});
	});

	/*// add a button "New" to add a new row
	var $appendButtonNew = $('<a id="ToolTables_pat_studyListTable_7" class="btn"><a/>'),
	$appendNameNew = $('<span>Add</span>');
	$('#pat_studyListInsertTable').find('.btn-group').append($appendButtonNew);
	$('#ToolTables_pat_studyListTable_7').append($appendNameNew);*/

	//add a button "Bulk Edit" to edit selected items
	var $appendButtonEdit = $('<a href="#" id="ToolTables_pat_studyListTable_8" class="btn"><a/>'),
	$appendNameEdit = $('<span>Upate Patient Study Status</span>');
	$('#pat_studyListInsertTable').find('.btn-group').append($appendButtonEdit);
	$('#ToolTables_pat_studyListTable_8').append($appendNameEdit);

	/*
	// add a calendar
	var $appendButtonCalendar = $('<a href="calendar.html" id="ToolTables_pat_studyListTable_9" class="btn"><a/>'),
	$appendNameCalendar = $('<span>Calendar View</span>');
	$('#pat_studyListInsertTable').find('.btn-group').append($appendButtonCalendar);
	$('#ToolTables_pat_studyListTable_9').append($appendNameCalendar);*/


	var nEditing = null;

	//add new raw
	$('#ToolTables_pat_studyListTable_7').on('click', function() {
		//e.preventDefault();

		// if I am using checkboxes
		/*var aiNew = oTable.fnAddData( [ '<input type="checkbox" class="checkbox" name="checkbox">','', '', '', '', '', 
						'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );*/

		var aiNew = oTable.fnAddData( ['', '', '', '', '', '', '',
		                               '<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );

		var nRow = oTable.fnGetNodes( aiNew[0] );
		editRow( oTable, nRow );
		nEditing = nRow;
	});


	//delete a new row
	$('#pat_studyListTable a.delete').on('click', function (e) {
		e.preventDefault();

		var nRow = $(this).parents('tr')[0];
		oTable.fnDeleteRow( nRow );
		$('#myModal').modal('show');
	} );


	// edit a new raw within the table
	$(document).on('click', '#pat_studyListTable a.edit', function (e) {
		e.preventDefault();
		/* Get the row as a parent of the link that was clicked on */
		var nRow = $(this).parents('tr')[0];

		if ( nEditing !== null && nEditing != nRow ) {
			/* Currently editing - but not this row - restore the old before continuing to edit mode */
			restoreRow( oTable, nEditing );
			editRow( oTable, nRow );
			nEditing = nRow;
		}


		else if ( nEditing == nRow && this.innerHTML == "Save" ) {
			/* Editing this row and want to save it */
			saveRow( oTable, nEditing );
			nEditing = null;
			$('#myModal').modal('show');
		}

		else {
			/* No edit in progress - let's start one */
			editRow( oTable, nRow );
			nEditing = nRow;

		}
	} );

}); //comes with datatables


$(document).ready(function(){
	// cycling added by yevheniy and daisuke
	$('#ToolTables_pat_studyListTable_8').on('click', function() {


		//Create Edit DataSet
		//e.preventDefault();
		var oTT = TableTools.fnGetInstance( 'pat_studyListTable');
		var aData = oTT.fnGetSelectedData();
		var eData = aData;
		var nowRowNum = 0;
		var DataSet = [aData, nowRowNum, eData];
		//alert(DataSet);
		$("#pat_studyListCycleOne").data("DataSet", DataSet);
		//alert($('#pat_studyListCycleOne').data('DataSet'));


		/*alert(aData[0]);
							alert(aData[1]);
							alert(aData.length);*/

		// add the events (columns data) on top of the modal window. Remove if pressed delete. Remove data from eData array..
		//...so that when do not cycle through the deleted data
		/*var i;
							for (i=0; i<eData.length; i++) {

								var $fieldsetForm = ('<fieldset data-role="fieldcontain" id="removeFields'+i+'" class="fieldset"></fieldset>'),
									$label = $('<label for="name'+i+'" class="ui-hidden-accessible"></label>'),
			              			$inputlabel = $('<input type="text" name="name_label[]" id="name'+i+'">'),
						    		//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
						    		$inputbutton = $('<a href="#" id="btnDel'+i+'" class="btnDel" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="c" data-inline="true">Delete</a>'); 

								$('#formFields').prepend($fieldsetForm);
								$('#removeFields'+i).append($label, $inputlabel, $inputbutton);
								$('#name'+i).val(eData[i][0]);

								//any of the below solutions will work
								//$("#formFields").on('click', '.btnDel', function(e) {
									//$(this).closest("fieldset").remove();
								$('#btnDel'+i).live('click', function() { // deprecated starting jQuery 1.9 http://api.jquery.com/live/
									$(this).closest("fieldset").remove();
									var currentID = $(this).attr('id');
									var removePosition = currentID[currentID.length-1];
									eData.splice(removePosition, 1);

									//alert(aData[i]);

									//for (k=0; k<eData.length; k++) {
									//	alert (eData[k]);
									//}
								});
							}	*/	

		$('#pat_studyListModalBody').prepend('<div id="pat_studyListSlimScroll"></div>')				
		$('#pat_studyListSlimScroll').slimScroll({
			color: '#00f',
			size: '10px',
			height: '80px',
			alwaysVisible: true,
			position: 'right', 
			railVisible: true,
			distance: '5px',
		});




		var i;
		for (i=0; i<eData.length; i++) {

			var $pat_studyListInputAppend = ('<div id="removeFields'+i+'" class="input-append"></div>'),
			$pat_studyListInput = $('<input type="text" class="span2" id="name'+i+'">'),
			//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
			$inputbutton = $('<button id="btnDel'+i+'" class="btn" type="button"><i class="icon-remove-circle"></i></button>'); 

			$('#pat_studyListSlimScroll').append($pat_studyListInputAppend);
			$('#removeFields'+i).append($pat_studyListInput, $inputbutton);
			$('#name'+i).val(eData[i][0]);

			//any of the below solutions will work
			//$("#formFields").on('click', '.btnDel', function(e) {
			//$(this).closest("fieldset").remove();
			$(document).on('click', '#btnDel'+i, function() { // "live" is deprecated starting jQuery 1.9 http://api.jquery.com/live/
				$(this).closest("div").remove();
				var currentID = $(this).attr('id');
				var removePosition = currentID[currentID.length-1];
				eData.splice(removePosition, 1);

				//alert(aData[i]);

				/*for (k=0; k<eData.length; k++) {
										alert (eData[k]);
									}*/
			});


		}

		$("#pat_studyListCycleOne").modal();
		//erase cache on initial close
		$(document).on('click', '#pat_studyListClose', function(){
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#pat_studyListCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#pat_studyListCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});


	});

	$('#pat_studyListApplyToAll').on('click', function() {
		$("#pat_studyListConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="pat_studyListApplyToAllPreviewButton">Preview</a>')
		$("#cycle1").remove();
		$(this).remove();
	});

	// cycle through celected raws
	$("#cycle1").on('click', function() {

		// when i first cycle, remove those fields I added above. 
		//$(".fieldset").remove();


		// added hack to fix jQuery Mobile's internal bug - otherwise page (specifically conent of the dialog and data)..
		//..is added twice upon click event. If you find a better way, please, apply
		/*var beforeTimeStamp = $("#pat_studyListCycleOne").data("ts");
								//alert(beforeTimeStamp);
								if (beforeTimeStamp != undefined){
								    if (e.timeStamp - beforeTimeStamp < 100) {
									      return;
									}
								}
								//alert(e.timeStamp);
								$("#pat_studyListCycleOne").data("ts", e.timeStamp);*/

		//alert(DataSet[1]);
		//e.preventDefault();

		$("#pat_studyListApplyToAll").remove();
		$('.slimScrollDiv').remove();

		//alert( $('#edit_d1').data('DataSet'));
		var DataSet   = $('#pat_studyListCycleOne').data('DataSet');
		//alert(DataSet[1]);
		var eData     = DataSet[2];
		var nowRowNum = DataSet[1];
		var aData   = DataSet[0];
		var maxRowNum = aData.length;


		/*alert(nowRowNum);
								alert(maxRowNum);*/
		if (nowRowNum == maxRowNum-1) {


			//all of this to just add preview button upon the last cycle. Will simplify with time
			$("#cycle1").remove();
			/*

										$('#footerdialog1').append('<li class="ui-block-c" id="addpreviewli"></li>');
										$("#addpreviewli").append('<a href="#" id="addpreviewa" data-role="button" data-icon="bars" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="span" data-theme="a" class="ui-btn ui-btn-inline ui-btn-icon-left ui-btn-up-a" data-iconpos="left" data-inline="true"></a>'); 
										$("#addpreviewa").append('<span class="ui-btn-inner" id="addpreviewspan1"></span>');
										$("#addpreviewspan1").append('<span class="ui-btn-text" id="addpreviewspan2">Preview</span>');
										$("#addpreviewspan1").append('<span class="ui-icon ui-icon-bars ui-icon-shadow" id="addpreviewspan3">&nbsp;</span>');*/
			$("#pat_studyListConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="pat_studyListPreviewButton">Preview</a>');
		}

		//saving to eData	
		$('#text-c1').val(eData[nowRowNum][0]);
		$('#text-c2').val(eData[nowRowNum][1]);
		$('#text-c3').val(eData[nowRowNum][2]);
		$('#text-c4').val(eData[nowRowNum][3]);
		$('#text-c5').val(eData[nowRowNum][4]);
		/*$('#text-c6').val(eData[nowRowNum][5]);
		$('#text-c7').val(eData[nowRowNum][6]);*/

		$('#pat_studyListModalPagination').html("<p>"+(nowRowNum+1)+ " of "+eData.length+" </p>")


		//increment nowRow
		DataSet[1]++;

		$('#pat_studyListCycleOne').data("DataSet", DataSet);


		//erase cache on cycle close
		$(document).on('click', '#pat_studyListClose', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#pat_studyListCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#pat_studyListCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#pat_studyListCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});


	// add reason for change...more work will be needed as every time i leave the field a next textearea will be added...
	$('.reasonForChange').change(function(){
		$(this).after('<textarea rows="2" placeholder="Reason for change"></textarea>');
	});

	// confirm
	$(document).on('click', '.preview', function() {

		$('#pat_studyListCycleOne').modal('hide');
		//e.preventDefault();

		// same as above
		/*var beforeTimeStamp = $("#edit_d1").data("ts");
							if (beforeTimeStamp != undefined){
								if (e.timeStamp - beforeTimeStamp < 100) {
								return;
								}
							}
							//alert(e.timeStamp);
							$("#cheduleOfEventsCycleOne").data("ts", e.timeStamp);*/



		/*view confirm*/
		var DataSet   = $('#pat_studyListCycleOne').data('DataSet');
		var eData     = DataSet[0];
		for (var i = 0; i < eData.length; i++) {

			// insert preview data afte cycling is done
			$('<th>'+eData[i][0]+'</th>').insertAfter('#headerInsertAfter');
			$('<td>'+eData[i][0]+'</td>').insertAfter('#c1InsertAfter');
			$('<td>'+eData[i][1]+'</td>').insertAfter('#c2InsertAfter');
			$('<td>'+eData[i][2]+'</td>').insertAfter('#c3InsertAfter');
			$('<td>'+eData[i][3]+'</td>').insertAfter('#c4InsertAfter');
			/*$('<td>'+eData[i][4]+'</td>').insertAfter('#c5InsertAfter');
			$('<td>'+eData[i][5]+'</td>').insertAfter('#c6InsertAfter');
			$('<td>'+eData[i][6]+'</td>').insertAfter('#c7InsertAfter');
			 */

		}

		$("#pat_studyListConfirm").modal();


		/*$(document).on('click', '#pat_studyListClose', function(){
								eData=[]; 
								aData=[]; 
								DataSet = [[], 0, []];
								$('.input-append').empty();
								$('#pat_studyListCycleOne').modal('hide');

							});*/

		//erase cache on closing preview
		$(document).on('click', '.closeEraseCache', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#pat_studyListCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#pat_studyListCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#pat_studyListCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});

	/*var inputControl = $('#pat_studyListTable_filter').find('label').html();
	$('#pat_studyListTable_filter').find('label').html(inputControl.replace('Search: ', ''));*/
	$('#pat_studyListTable_filter').find('input').attr('placeholder', 'Search List');
	$('#pat_studyListTable_filter').append('<img src = "img/advanced_search.gif" id="ad_search"/>').css('');
	//$('#pat_studyListTable_filter').find('label').replaceWith('<label><input type="text" aria-controls="pat_studyListTable"></label>');

	/*var label =$('<img/>');
	 label.append('img/advanced_search.gif');
	 label.css('float: right');*/


	//$(".second").pageslide({ direction: "left", modal: true });


	// handling submit buttons
	$(document).on('click', '#submitButton', function() {

		// submit validation somewhere here
		$('#myModal').modal('hide');

	});

	$(document).on('click', '#submitPreviewButton', function() {

		// submit validation somewhere here
		$('#pat_studyListConfirm').modal('hide');

	});


	$('.ColVis_MasterButton').addClass('btn btn-primary');

	/*$(document).on('click', '#slidingNotesLaningView', function() {
					alert('hi');
					$.pageslide({direction:'left', href:'slidingnotes.html'});
			});*/


	// sorting of accordian
	/*
	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();*/


} ); //document.ready close


//-----------------------------------------Schedule List--------------------------------------------------------------

$(document).ready(function() {
	$('#scheduleOfEventsInsertTable').html( '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="scheduleOfEventsTable"></table>' );
	var oTable = $('#scheduleOfEventsTable').dataTable( {

		"aaData": [
		           //Reduced data set - next step is to feed this variable with incoming/parsed .json or xml
		           [ "","Baseline", "10002", "TRF8201", "Not Done", "Evaluation Visit 1", "Done", "Mar 4, 2013" ],
		           [ "","Evaluation", "01004", "NCT2001","Done","Initial Visit", "Done", "Mar 4, 2013",   ],
		           [ "","Hemoglobin Count", "03067", "JKR0021","Not Required","Follow-up Visit 1", "Done", "Mar 4, 2013",],
		           [ "","Anesthesia", "15234", "NCT2984","Not Required","Dental Cleaning 1", "Done", "Mar 15, 2013",],
		           [ "","Injection", "82234", "TRF8822" ,"Done", "Dental Cleaning 1", "Done", "Mar 15, 2013"],
		           [ "","Hemoglobin Count", "12367", "SGF0021","Done","Dental Cleaning 1", "Done", "Mar 15, 2013",],
		           [ "","X-ray", "14123", "PRW5501","Done" , "Evaluation Visit", "Not Required", "Apr 2, 2013"],
		           [ "","Baseline", "21334", "NCT29484","Done", "Initial Visit", "Not Done", "Apr 2, 2013",],
		           [ "","Evaluation", "53256", "NCT29484","Not Done", "Dental Cleaning 2", "Not Required", "Apr 2, 2013",],
		           [ "","Anesthesia", "84927", "NCT00321" ,"Done", "Dental Cleaning 2", "Done", "Apr 2, 2013",],
		           [ "","Injection", "98762", "KE290001" ,"Done", "Dental Cleaning 2", "Not Done", "Apr 2, 2013"],
		           [ "","Hemoglobin Count", "12343", "TRF8823" ,"Done", "Lab Testing", "Done", "Apr 14, 2013"],
		           [ "","X-ray", "10098", "JFRK003" ,"Done", "Dental Cleaning 3", "Not Done", "Apr 14, 2013"],
		           [ "","Injection", "34876", "NCT2004" ,"Done", "Dental Cleaning 3", "Not Required", "Apr 21, 2013"]
		           ],
		           //column names will be handled a bit different...for now like this, later should be based on the incoming json
		           "aoColumnDefs"  : [
		                              {
		                            	  aTargets: [0],    // Column number which needs to be modified
		                            	  fnRender: function (o, v) {   // o, v contains the object and value for the column
		                            		  return '<input type="checkbox" id="selectall" name="selectall" />';
		                            	  },
		                            	  sClass: 'tableCell'    // Optional - class to be applied to this table cell
		                              }],
		                              "aoColumns": [
		                                            {"sTitle": "<input type='checkbox'></input>","mDataProp": null, 
		                                            	"sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>", 
		                                            	"bSortable": false},
		                                            	{ "sTitle": "Event Name" },
		                                            	{ "sTitle": "Patient ID" },
		                                            	{ "sTitle": "Study ID" },
		                                            	{ "sTitle": "Event Status" },
		                                            	{ "sTitle": "Visit Name", "sClass": "center" },
		                                            	{ "sTitle": "Visit Status"},
		                                            	{ "sTitle": "Scheduled Date" },
		                                            	/*{ "sTitle": "Edit"}, 
		                                            	{ "sTitle": "Delete"},*/
		                                            	],

		                                            	"sDom": "R<'span2'C><'row'<'span3'l><'span6'T><'span2'f>r>t<'row'<'span6'i><'span6'p>>",
		                                            	"sPaginationType": "bootstrap",
		                                            	"oLanguage": {
		                                            		"sLengthMenu": "_MENU_ records per page"
		                                            	},
		                                            	"aaSorting":[[0, "asc"]],
		                                            	"iDisplayLength": 10,
		                                            	"oTableTools": {
		                                            		//"sSwfPath": "http://localhost/copy_csv_xls_pdf.swf",
		                                            		"sRowSelect": "multi",
		                                            		//"sSelectedClass": "row_selected",
		                                            		"aButtons": [
		                                            		             "copy",
		                                            		             "print",
		                                            		             {
		                                            		            	 "sExtends":    "collection",
		                                            		            	 "sButtonText": 'Export <span class="caret" />',
		                                            		            	 "aButtons":    [ "csv", "xls", "pdf" ]
		                                            		             }, 
		                                            		             {
		                                            		            	 "sExtends":    "collection",
		                                            		            	 "sButtonText": 'Select <span class="caret" />',
		                                            		            	 "aButtons":    [ "select_all", "select_none"]
		                                            		             },
		                                            		             ]
		                                            	}, 


		                                            	/* "bProcessing": false,
			        "bServerSide": true,
			        "sAjaxSource": "scripts/server_processing.php",
			        "fnDrawCallback": function () {
			            $('#example tbody td').editable( '../examples_support/editable_ajax.php', {
			                "callback": function( sValue, y ) {
			                    /* Redraw the table from the new data on the server 
			                    oTable.fnDraw();
			                },
			                "height": "14px"
			            } );
			        }*/
	} );



	$('button').addClass('btn DTTT_button_collection');

	$(document).on('click', '.ColVis_MasterButton', function(){
		$('.ColVis_collection').find('button').addClass('btn btn-india').css({'width':'150px'}).css({'margin-buttom':'0.9px'});
		$(".ColVis_radio").find('input').css({'float':'left'}).css({'margin':'6px 7px 0px 0px'});
		$(".ColVis_title").css({'float':'left'});
	});

	// add a button "New" to add a new row
	var $appendButtonNew = $('<a id="ToolTables_scheduleOfEventsTable_7" class="btn"><a/>'),
	$appendNameNew = $('<span>Add Event</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonNew);
	$('#ToolTables_scheduleOfEventsTable_7').append($appendNameNew);

	//add a button "Bulk Edit" to edit selected items
	var $appendButtonEdit = $('<a href="#" id="ToolTables_scheduleOfEventsTable_8" class="btn"><a/>'),
	$appendNameEdit = $('<span>Bulk Edit</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonEdit);
	$('#ToolTables_scheduleOfEventsTable_8').append($appendNameEdit);


	// add a calendar
	var $appendButtonCalendar = $('<a href="calendar.html" id="ToolTables_scheduleOfEventsTable_9" class="btn"><a/>'),
	$appendNameCalendar = $('<span>Calendar View</span>');
	$('#scheduleOfEventsInsertTable').find('.btn-group').append($appendButtonCalendar);
	$('#ToolTables_scheduleOfEventsTable_9').append($appendNameCalendar);


	var nEditing = null;

	//add new raw
	$('#ToolTables_scheduleOfEventsTable_7').on('click', function() {
		//e.preventDefault();

		// if I am using checkboxes
		/*var aiNew = oTable.fnAddData( [ '<input type="checkbox" class="checkbox" name="checkbox">','', '', '', '', '', 
						'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );*/

		var aiNew = oTable.fnAddData( ['', '', '', '', '', '', '',
		                               '<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );

		var nRow = oTable.fnGetNodes( aiNew[0] );
		editRow( oTable, nRow );
		nEditing = nRow;
	});


	//delete a new row
	$('#scheduleOfEventsTable a.delete').on('click', function (e) {
		e.preventDefault();

		var nRow = $(this).parents('tr')[0];
		oTable.fnDeleteRow( nRow );
		$('#myModal').modal('show');
	} );


	// edit a new raw within the table
	$(document).on('click', '#scheduleOfEventsTable a.edit', function (e) {
		e.preventDefault();
		/* Get the row as a parent of the link that was clicked on */
		var nRow = $(this).parents('tr')[0];

		if ( nEditing !== null && nEditing != nRow ) {
			/* Currently editing - but not this row - restore the old before continuing to edit mode */
			restoreRow( oTable, nEditing );
			editRow( oTable, nRow );
			nEditing = nRow;
		}


		else if ( nEditing == nRow && this.innerHTML == "Save" ) {
			/* Editing this row and want to save it */
			saveRow( oTable, nEditing );
			nEditing = null;
			$('#myModal').modal('show');
		}

		else {
			/* No edit in progress - let's start one */
			editRow( oTable, nRow );
			nEditing = nRow;

		}
	} );



}); //comes with datatables


$(document).ready(function(){
	// cycling added by yevheniy and daisuke
	$('#ToolTables_scheduleOfEventsTable_8').on('click', function() {


		//Create Edit DataSet
		//e.preventDefault();
		var oTT = TableTools.fnGetInstance( 'scheduleOfEventsTable');
		var aData = oTT.fnGetSelectedData();
		var eData = aData;
		var nowRowNum = 0;
		var DataSet = [aData, nowRowNum, eData];
		//alert(DataSet);
		$("#scheduleOfEventsCycleOne").data("DataSet", DataSet);
		//alert($('#scheduleOfEventsCycleOne').data('DataSet'));


		/*alert(aData[0]);
							alert(aData[1]);
							alert(aData.length);*/

		// add the events (columns data) on top of the modal window. Remove if pressed delete. Remove data from eData array..
		//...so that when do not cycle through the deleted data
		/*var i;
							for (i=0; i<eData.length; i++) {

								var $fieldsetForm = ('<fieldset data-role="fieldcontain" id="removeFields'+i+'" class="fieldset"></fieldset>'),
									$label = $('<label for="name'+i+'" class="ui-hidden-accessible"></label>'),
			              			$inputlabel = $('<input type="text" name="name_label[]" id="name'+i+'">'),
						    		//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
						    		$inputbutton = $('<a href="#" id="btnDel'+i+'" class="btnDel" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="c" data-inline="true">Delete</a>'); 

								$('#formFields').prepend($fieldsetForm);
								$('#removeFields'+i).append($label, $inputlabel, $inputbutton);
								$('#name'+i).val(eData[i][0]);

								//any of the below solutions will work
								//$("#formFields").on('click', '.btnDel', function(e) {
									//$(this).closest("fieldset").remove();
								$('#btnDel'+i).live('click', function() { // deprecated starting jQuery 1.9 http://api.jquery.com/live/
									$(this).closest("fieldset").remove();
									var currentID = $(this).attr('id');
									var removePosition = currentID[currentID.length-1];
									eData.splice(removePosition, 1);

									//alert(aData[i]);

									//for (k=0; k<eData.length; k++) {
									//	alert (eData[k]);
									//}
								});
							}	*/	

		$('#scheduleOFEventsModalBody').prepend('<div id="scheduleOfEventsSlimScroll"></div>')				
		$('#scheduleOfEventsSlimScroll').slimScroll({
			color: '#00f',
			size: '10px',
			height: '80px',
			alwaysVisible: true,
			position: 'right', 
			railVisible: true,
			distance: '5px',
		});




		var i;
		for (i=0; i<eData.length; i++) {

			var $scheduleOfEventsInputAppend = ('<div id="removeFields'+i+'" class="input-append"></div>'),
			$scheduleOfEventsInput = $('<input type="text" class="span2" id="name'+i+'">'),
			//$inputbutton = $('<input class="btnDel" id="btnDel'+i+'" type="button" value="remove name">');
			$inputbutton = $('<button id="btnDel'+i+'" class="btn" type="button"><i class="icon-remove-circle"></i></button>'); 

			$('#scheduleOfEventsSlimScroll').append($scheduleOfEventsInputAppend);
			$('#removeFields'+i).append($scheduleOfEventsInput, $inputbutton);
			$('#name'+i).val(eData[i][0]);

			//any of the below solutions will work
			//$("#formFields").on('click', '.btnDel', function(e) {
			//$(this).closest("fieldset").remove();
			$(document).on('click', '#btnDel'+i, function() { // "live" is deprecated starting jQuery 1.9 http://api.jquery.com/live/
				$(this).closest("div").remove();
				var currentID = $(this).attr('id');
				var removePosition = currentID[currentID.length-1];
				eData.splice(removePosition, 1);

				//alert(aData[i]);

				/*for (k=0; k<eData.length; k++) {
										alert (eData[k]);
									}*/
			});


		}

		$("#scheduleOfEventsCycleOne").modal();
		//erase cache on initial close
		$(document).on('click', '#scheduleOfEventsClose', function(){
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});


	});

	$('#scheduleOfEventsApplyToAll').on('click', function() {
		$("#scheduleOfEventsConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="scheduleOfEventsApplyToAllPreviewButton">Preview</a>')
		$("#cycle1").remove();
		$(this).remove();
	});

	// cycle through celected raws
	$("#cycle1").on('click', function() {

		// when i first cycle, remove those fields I added above. 
		//$(".fieldset").remove();


		// added hack to fix jQuery Mobile's internal bug - otherwise page (specifically conent of the dialog and data)..
		//..is added twice upon click event. If you find a better way, please, apply
		/*var beforeTimeStamp = $("#scheduleOfEventsCycleOne").data("ts");
								//alert(beforeTimeStamp);
								if (beforeTimeStamp != undefined){
								    if (e.timeStamp - beforeTimeStamp < 100) {
									      return;
									}
								}
								//alert(e.timeStamp);
								$("#scheduleOfEventsCycleOne").data("ts", e.timeStamp);*/

		//alert(DataSet[1]);
		//e.preventDefault();

		$("#scheduleOfEventsApplyToAll").remove();
		$('.slimScrollDiv').remove();

		//alert( $('#edit_d1').data('DataSet'));
		var DataSet   = $('#scheduleOfEventsCycleOne').data('DataSet');
		//alert(DataSet[1]);
		var eData     = DataSet[2];
		var nowRowNum = DataSet[1];
		var aData   = DataSet[0];
		var maxRowNum = aData.length;


		/*alert(nowRowNum);
								alert(maxRowNum);*/
		if (nowRowNum == maxRowNum-1) {


			//all of this to just add preview button upon the last cycle. Will simplify with time
			$("#cycle1").remove();
			/*

										$('#footerdialog1').append('<li class="ui-block-c" id="addpreviewli"></li>');
										$("#addpreviewli").append('<a href="#" id="addpreviewa" data-role="button" data-icon="bars" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="span" data-theme="a" class="ui-btn ui-btn-inline ui-btn-icon-left ui-btn-up-a" data-iconpos="left" data-inline="true"></a>'); 
										$("#addpreviewa").append('<span class="ui-btn-inner" id="addpreviewspan1"></span>');
										$("#addpreviewspan1").append('<span class="ui-btn-text" id="addpreviewspan2">Preview</span>');
										$("#addpreviewspan1").append('<span class="ui-icon ui-icon-bars ui-icon-shadow" id="addpreviewspan3">&nbsp;</span>');*/
			$("#scheduleOfEventsConfirmFooterButton").append('<a href="#" class="btn btn-success preview" id="scheduleOfEventsPreviewButton">Preview</a>');
		}

		//saving to eData	
		$('#text-c1').val(eData[nowRowNum][0]);
		$('#text-c2').val(eData[nowRowNum][1]);
		$('#text-c3').val(eData[nowRowNum][2]);
		$('#text-c4').val(eData[nowRowNum][3]);
		$('#text-c5').val(eData[nowRowNum][4]);
		$('#text-c6').val(eData[nowRowNum][5]);
		$('#text-c7').val(eData[nowRowNum][6]);

		$('#scheduleOfEventsModalPagination').html("<p>"+(nowRowNum+1)+ " of "+eData.length+" </p>")


		//increment nowRow
		DataSet[1]++;

		$('#scheduleOfEventsCycleOne').data("DataSet", DataSet);


		//erase cache on cycle close
		$(document).on('click', '#scheduleOfEventsClose', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#scheduleOfEventsCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});


	// add reason for change...more work will be needed as every time i leave the field a next textearea will be added...
	$('.reasonForChange').change(function(){
		$(this).after('<textarea rows="2" placeholder="Reason for change"></textarea>');
	});

	// confirm
	$(document).on('click', '.preview', function() {

		$('#scheduleOfEventsCycleOne').modal('hide');
		//e.preventDefault();

		// same as above
		/*var beforeTimeStamp = $("#edit_d1").data("ts");
							if (beforeTimeStamp != undefined){
								if (e.timeStamp - beforeTimeStamp < 100) {
								return;
								}
							}
							//alert(e.timeStamp);
							$("#cheduleOfEventsCycleOne").data("ts", e.timeStamp);*/



		/*view confirm*/
		var DataSet   = $('#scheduleOfEventsCycleOne').data('DataSet');
		var eData     = DataSet[0];
		for (var i = 0; i < eData.length; i++) {

			// insert preview data afte cycling is done
			$('<th>'+eData[i][0]+'</th>').insertAfter('#headerInsertAfter');
			$('<td>'+eData[i][0]+'</td>').insertAfter('#c1InsertAfter');
			$('<td>'+eData[i][1]+'</td>').insertAfter('#c2InsertAfter');
			$('<td>'+eData[i][2]+'</td>').insertAfter('#c3InsertAfter');
			$('<td>'+eData[i][3]+'</td>').insertAfter('#c4InsertAfter');
			$('<td>'+eData[i][4]+'</td>').insertAfter('#c5InsertAfter');
			$('<td>'+eData[i][5]+'</td>').insertAfter('#c6InsertAfter');
			$('<td>'+eData[i][6]+'</td>').insertAfter('#c7InsertAfter');


		}

		$("#scheduleOfEventsConfirm").modal();


		/*$(document).on('click', '#scheduleOfEventsClose', function(){
								eData=[]; 
								aData=[]; 
								DataSet = [[], 0, []];
								$('.input-append').empty();
								$('#scheduleOfEventsCycleOne').modal('hide');

							});*/

		//erase cache on closing preview
		$(document).on('click', '.closeEraseCache', function(){

			//cleanCache (eData, aData, DataSet,'..slimScrollDiv','#scheduleOfEventsCycleOne');	
			//cleanCache (eData, aData, DataSet, '.slimScrollDiv','#scheduleOfEventsCycleOne');
			/*eData = [];
									aData =[];
									DataSet=[eData, 0, aData];
									//alert('ready to remove');
									$('.slimScrollDiv').remove(); // fix this*/
			//alert('removed');
			$('#scheduleOfEventsCycleOne').modal('hide');
			//alert(z); needs fixing -> the result is not showing when i am using this function...
			//alert(DataSet);
			location.reload();
		});

	});

	$('#scheduleOfEventsTable_filter').find('input').attr('placeholder', 'Search List');

	//$(".second").pageslide({ direction: "left", modal: true });


	// handling submit buttons
	$(document).on('click', '#submitButton', function() {

		// submit validation somewhere here
		$('#myModal').modal('hide');

	});

	$(document).on('click', '#submitPreviewButton', function() {

		// submit validation somewhere here
		$('#scheduleOfEventsConfirm').modal('hide');

	});


	$('.ColVis_MasterButton').addClass('btn btn-primary');

	/*$(document).on('click', '#slidingNotesLaningView', function() {
					alert('hi');
					$.pageslide({direction:'left', href:'slidingnotes.html'});
			});*/

} ); //document.ready close




//-----------------------------------------AJAX Calls-------------------------------------------------------------
$(document).ready(function(){
	$('[data-slidepanel]').slidepanel({
		orientation: 'left',
		mode: 'push'
	});
	$('body').css({
		backgroundColor: '#FFFFFF',
		color: '#333333',
		fontFamily:' "Helvetica Neue",Helvetica,Arial,sans-serif',
		fontSize: '14px',
		lineHeight: '20px',
		margin: 0
	});

	callAjaxProxyForPatient();

});


//ajax call 
function makeBasicAuth(user, pass) {
	var tok = 'Basic '+user + ':' + pass;
	//var tok = Base64.encode(tok);
	return tok;
}

function callAjaxProxyForStudy() {
	var myData = {
			"studyNumber":$("#inputStudyNumber").val()
	};
	var param = 'input='+YAHOO.lang.JSON.stringify(myData);

	$.ajax({
		cache: true,
		url: "/velos-e3/clientproxy/remote/http+216.218.236.30/studyservice/getStudy",
		data: param,
		type: "POST",
		dataType:'json',
		beforeSend : function(req) {
			req.setRequestHeader('Authorization', makeBasicAuth('kchetal', 'velos123'));
		},
		success:function(resp){
			if (!resp) { return; }
			if (resp.issues && resp.issues.issue) {
				for (var iX=0; iX<resp.issues.issue.length; iX++) {
					alert(resp.issues.issue[iX].message);
				}
				return;
			}
			if (resp.study && resp.study.studySummary && resp.study.studySummary.studyTitle) {
				alert(resp.study.studySummary.studyTitle);
			}
		}
	});
}
function callAjaxProxyForPatient() {
	//alert("Test Done");
	var myData = {
			"patientId":$("#inputPatientId").val(),
			"organizationId":{
				"siteAltId":$("#inputSiteAltId").val(),
				"siteName":$("#inputSiteName").val()
			}
	};
	//alert("Test data");
	//var param = 'input='+YAHOO.lang.JSON.stringify(myData);
	var param = 'input={"patientId":"'+$("#inputPatientId").val()+'","organizationId":{"siteName":"'+$("#inputSiteName").val()+'"}}'; 
	// alert("Test data1");
	$.ajax({
		cache: true,
		url: "/velos-e3/clientproxy/remote/http+216.218.236.30/patientdemographicsservice/getPatientDetails",
		data: param,
		type: "POST",
		dataType:'json',
		beforeSend : function(req) {
			//alert("Test b4send");
			req.setRequestHeader('Authorization', makeBasicAuth('kchetal', 'velos123'));
			//alert("Test b4sendg");
		},
		success:function(resp){

			//alert(formatDate(resp.patientDemographics.registrationDate));
			if (!resp) { return; }
			if (resp.issues && resp.issues.issue) {
				for (var iX=0; iX<resp.issues.issue.length; iX++) {
					alert(resp.issues.issue[iX].message);
				}
				return;
			}
			$('#firstName').val(resp.patientDemographics.firstName);
			$('#middleName').val(resp.patientDemographics.middleName);
			$('#lastName').val(resp.patientDemographics.lastName);
			$('#inputPatientId').val(resp.patientDemographics.inputPatientId);
			$('#survivalStatus').val(resp.patientDemographics.survivalStatus);
			$('#gender').val(resp.patientDemographics.gender.description);
			$('#maritalStatus').val(resp.patientDemographics.maritalStatus);
			$('#bloodGroup').val(resp.patientDemographics.bloodGroup);
			$('#ethnicity').val(resp.patientDemographics.ethnicity.description);
			$('#race').val(resp.patientDemographics.race.description);
			/*$('#ssn').val(resp.PatientDemographics.SSNNot.toString());*/
			$('#address1').val(resp.patientDemographics.address1);
			$('#address2').val(resp.patientDemographics.address2);
			$('#city').val(resp.patientDemographics.city);
			$('#county').val(resp.patientDemographics.county);
			$('#zipCode').val(resp.patientDemographics.zipCode);
			$('#country').val(resp.patientDemographics.country);
			$('#timezone').val(resp.patientDemographics.timeZone.description);
			$('#homePhone').val(resp.patientDemographics.homePhone);
			$('#workPhone').val(resp.patientDemographics.workPhone);
			$('#inputsiteName').val(resp.patientDemographics.inputsiteName);
			$('#facilityID').val(resp.patientDemographics.organization.ID);
			$('#registrationDate').val( formatDate(resp.patientDemographics.registrationDate));
			$('#provider').val(resp.patientDemographics.provider);
			$('#g_department').val(resp.patientDemographics.g_department);
			$('#notes').val(resp.patientDemographics.notes);
		}
	});
}
function callAjaxProxyForPatientSchedule() {
	var myData = {
			"patientIdentifier":{
				"patientId":$("#inputPatientId").val(),
				"organizationId":{
					"siteAltId":$("#inputSiteAltId").val(),
					"siteName":$("#inputSiteName").val()
				}
			},
			"studyIdentifier":{
				"studyNumber":$("#inputStudyNumber").val()
			}
	};
	var param = 'input='+YAHOO.lang.JSON.stringify(myData);
	//var param = 'input={"patientId":"'+patientId+'","organizationId":{"siteAltId":"'+siteAltId+'","siteName":"'+siteName+'"}}';
	$.ajax({
		cache: true,
		url: "/velos-e3/clientproxy/remote/http+216.218.236.30/patientscheduleservice/getCurrentPatientSchedule",
		data: param,
		type: "POST",
		dataType:'json',
		beforeSend : function(req) {
			req.setRequestHeader('Authorization', makeBasicAuth('kchetal', 'velos123'));
		},
		success:function(resp){
			if (!resp) { return; }
			if (resp.issues && resp.issues.issue) {
				for (var iX=0; iX<resp.issues.issue.length; iX++) {
					alert(resp.issues.issue[iX].message);
				}
				return;
			}
			alert(resp.patientSchedule.patientScheduleSummary.calendarName);
		}
	});
}




//convert json into arrayofarrays - for now it only works for json of jsons, however, patient event schedule comes with json of arrays of json..blahblah
function getArrayFromJSON(obj, retArray) {
	for (var key in obj) {
		if(isObject(obj[key]) == true) {
			var aData = [];
			getArrayFromJSON(obj[key], aData);
			retArray.push(aData);
		}
		else {
			retArray.push(obj[key]);
		}
	}
}
function isObject(obj) {
	return obj instanceof Object && Object.getPrototypeOf(obj) === Object.prototype;
}

$(document).ready(function(){
	$(".datepicker").datepicker();

	$("#other_info").click(function () {
		$("#toggle_info").toggle("slow");
	});
});