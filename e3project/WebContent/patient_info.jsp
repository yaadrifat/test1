
<%
	String patId = request.getParameter("patId");
    String siteName = request.getParameter("siteName");
	System.out.println("patId =" + patId+ "siteName=" +siteName);
%>
<div class="accordion-group">
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#accordion" href="#collapseTwo"> Patient Information
		</a>
	</div>

	<div id="collapseTwo" class="accordion-body collapse in">
		<div class="accordion-inner">
			<div class="container">
				<div class="span5 patientCanvasFields">
					<!-- <h4 style="text-align: center">Personal</h4> -->
					<form class="form-horizontal">
						<div class="control-group">
							<label class="control-label" for="firstName">First Name</label>
							<div class="controls">
								<input type="text" id="firstName" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="middleName">Middle Name</label>
							<div class="controls">
								<input type="text" id="middleName" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="lastName">Last Name</label>
							<div class="controls">
								<input type="text" id="lastName" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="inputPatientId">Patient
								Id</label>
							<div class="controls">
								<input type="hidden" id="inputPatientId" value="<%=patId%>" />
								<!-- <button type="button" name="patientSearchButton"
									onClick="callAjaxProxyForPatient();">Search Patient</button> -->
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="survivalstatus">Survival
								Status</label>
							<div class="controls">
								<input type="text" id="survivalstatus" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="dob">Date of Birth</label>
							<div class="controls">
								<input type="text" id="dob" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="gender">Gender</label>
							<div class="controls">
								<input type="text" id="gender" placeholder="" />
							</div>
						</div>
					</form>
				</div>

				<div class="span5">

					<form class="form-horizontal" id="span 12">
						<div class="control-group">
							<label class="control-label" for="maritalStatus">Marital
								Status</label>
							<div class="controls">
								<input type="text" id="maritalStatus" placeholder="">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="bloodGroup">Blood Group</label>
							<div class="controls">
								<input type="text" id="bloodGroup" placeholder="" />
							</div>
						</div>

						<!-- <div class="control-group">
							<label class="control-label" for="ssn">SSN</label>
							<div class="controls">
								<input type="text" id="ssn" placeholder="" />
							</div>
						</div>
                        -->
						<div class="control-group">
							<label class="control-label" for="ethnicity">Ethnicity</label>
							<div class="controls">
								<input type="text" id="ethnicity" placeholder="" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="race">Race</label>
							<div class="controls">
								<input type="text" id="race" placeholder="" />
							</div>
						</div>


					</form>
				</div>
			</div>
			<div>
				<a href="#" id="other_info">Click here for more information</a>
			</div>
			<!-- <img id="other_info" src= "img/more_info.png" /> -->
			<div id="toggle_info">

				<div class="span5">
					<form class="form-horizontal" id="span 12">
						<h4 style="text-align: center">Contact</h4>
						<div class="control-group">
							<label class="control-label" for="address1">Address 1</label>
							<div class="controls">
								<input type="text" id="address1" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="address2">Address 2</label>
							<div class="controls">
								<input type="text" id="address2" placeholder="N/A" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="city">City</label>
							<div class="controls">
								<input type="text" id="city" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="county">County</label>
							<div class="controls">
								<input type="text" id="county" placeholder="" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="zipCode">Zip/Postal
								Code</label>
							<div class="controls">
								<input type="text" id="zipCode" placeholder="" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="country">Country</label>
							<div class="controls">
								<input type="text" id="country" placeholder="" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="timezone">Time Zone</label>
							<div class="controls">
								<input type="text" id="timezone" placeholder="" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="homePhone">Home Phone</label>
							<div class="controls">
								<input type="text" id="homePhone" placeholder="" />
							</div>
						</div>


						<div class="control-group">
							<label class="control-label" for="wphone">Work Phone</label>
							<div class="controls">
								<input type="text" id="wphone" placeholder="N/A" />
							</div>
						</div>

					</form>


				</div>
				<div class="span5">
					<h4 style="text-align: center">Medical</h4>
					<form class="form-horizontal" id="span 12">
						<div class="control-group">
							<label class="control-label" for="inputSiteName">Organization</label>
							<div class="controls">
								<input type="hidden" id="inputSiteName" value="<%=siteName %>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="facilityID">Facility ID</label>
							<div class="controls">
								<input type="number" id="facilityID" placeholder="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registrationDate">Registration
								Date</label>
							<div class="controls">
								<input type="date" class="datepicker" id="registrationDate"
									placeholder="" />
								<!-- <span class="add-on"><i class="icon-th"></i></span> -->
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="provider">Provider</label>
							<div class="controls">
								<input type="text" id="provider" placeholder="Private" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="g_departement">Group
								Department</label>
							<div class="controls">
								<input type="text" id="g_department" placeholder="N/A" />
							</div>
						</div>
					</form>
					<form class="form-horizontal" id="span 12">

						<h4 style="text-align: center">Notes</h4>

						<div class="control-group">
							<div class="controls">
								<textarea id="notes" style="width: auto;"
									placeholder="notes here" data-provide="limit"></textarea>
							</div>
						</div>

					</form>
				</div>
			</div>
			<!-- toggle div -->
		</div>
		<!--End of -inner-->
	</div>
	<!--End of accordion collapse2 div-->
</div>
<!--End of accordian div-->
