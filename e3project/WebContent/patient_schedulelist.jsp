
<div class="accordion-group">
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#scheduleOfEventsListAccordion" href="#collapseOne"> Schedule
			List </a>
	</div>
	<div id="collapseOne" class="accordion-body collapse in">
		<div class="accordion-inner">
			<div class="container">
				<div id="scheduleOfEventsInsertTable"></div>
			</div>
		</div>
	</div>
</div>
