
<div id="pat_studyListCycleOne" class="modal hide fade in"
	style="display: none;">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">x</a>
		<h3>Edit Patient Study List</h3>
	</div>
	<div class="modal-body" id="pat_studyListModalBody">


		<form method='get' action='' id='pat_studyListFormFields'
			class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="text-c1" id="c1">Study
					Number</label>
				<div class="controls">
					<input type="text" id="text-c1" class="reasonForChange">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="text-c2" id='c2'>Study
					Title</label>
				<div class="controls">
					<input type="text" id="text-c2" class="reasonForChange">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="text-c3" id='c3'>Patient
					Study Status</label>
				<div class="controls">
					<input type="text" id="text-c3" class="reasonForChange">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="text-c4" id="c4">Study
					Status</label>
				<div class="controls">
					<input type="text" id="text-c4" class="reasonForChange">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="text-c5" id="c5">Principal
					Investigator</label>
				<div class="controls">
					<input type="text" id="text-c5" class="reasonForChange">
				</div>
			</div>

			<!-- <div class="control-group">
				<label class="control-label" for="text-c6" id="c6">Start
					Date</label>
				<div class="controls">
					<input type="text" id="text-c6" class="reasonForChange">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="text-c7" id="c7">End Date</label>
				<div class="controls">
					<input type="text" id="text-c7" class="reasonForChange">
				</div>
			</div> -->
		</form>

	</div>
	<div class="modal-footer" id="pat_studyListConfirmFooterButton">
		<div id='pat_studyListModalPagination'
			class="pagination pagination-right"></div>
		<a href="#" class="btn btn-info" id="pat_studyListBack">Back</a> <a
			href="#" class="btn btn-danger closeEraseCache"
			id="pat_studyListClose">Cancel</a> <a href="#"
			class="btn btn-inverse" id="pat_studyListApplyToAll">Apply To All</a>
		<a href="#" class="btn btn-success" id="cycle1">Next</a>
	</div>
</div>


<div id="pat_studyListConfirm" class="modal hide fade in"
	style="display: none;">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">x�</a>
		<h3>Preview and Sign</h3>
	</div>
	<div class="modal-body">
		<table id="tableconfirm1" class="table table-condensed">
			<thead>
				<tr>
					<th id="headerInsertAfter">Column Data</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="c1InsertAfter">Study Number</td>
				</tr>
				<tr>
					<td id="c2InsertAfter">Study Title</td>
				</tr>
				<tr>
					<td id="c3InsertAfter">Patient Study Status</td>
				</tr>
				<tr>
					<td id="c4InsertAfter">Study Status</td>
				</tr>
				<tr>
					<td id="c5InsertAfter">Principal Investigator</td>
				</tr>
				<!-- 
				<tr>
					<td id="c6InsertAfter">Suggested Date</td>
				</tr>

				<tr>
					<td id="c7InsertAfter">Schedule Date</td>
				</tr> -->

			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button class="btn span1 btn-danger closeEraseCache"
			data-dismiss="modal" aria-hidden="true"
			id="pat_studyListPreviewClose">Cancel</button>
		<div class="input-append">
			<input class="span2" id="appendedInputButton" type="text"
				placeholder='Sign'>
			<button class="btn btn-success closeEraseCache" type="button"
				id="submitPreviewButton">Submit</button>
		</div>
	</div>
</div>



<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">x</button>
		<h3 id="myModalLabel">Reason for change</h3>
	</div>
	<div class="modal-body">
		<textarea rows="2" class='span4'></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn span1 btn-danger closeEraseCache"
			data-dismiss="modal" aria-hidden="true">Cancel</button>
		<div class="input-append">
			<input class="span2" id="appendedInputButton" type="text"
				placeholder='Sign'>
			<button class="btn btn-success" type="button" id="submitButton">Submit</button>
		</div>
	</div>
</div>




<script type="text/javascript">
	/*$(document).ready(function() {
	    createStoryJS({
	        type:       'timeline',
	        width:      '800',
	        height:     '600',
	        source:     'timelineExample.json',
	        embed_id:   'my-timeline'
	    });
	});*/

	var timeline_config = {
		width : '100%',
		height : '600',
		source : 'timelineExamplePatient.json',
		embed_id : 'timeline-embed', //OPTIONAL USE A DIFFERENT DIV ID FOR EMBED
		start_at_end : false, //OPTIONAL START AT LATEST DATE
		start_at_slide : '4', //OPTIONAL START AT SPECIFIC SLIDE
		start_zoom_adjust : '3', //OPTIONAL TWEAK THE DEFAULT ZOOM LEVEL
		hash_bookmark : true, //OPTIONAL LOCATION BAR HASHES
		font : 'Bevan-PotanoSans', //OPTIONAL FONT
		debug : true, //OPTIONAL DEBUG TO CONSOLE
		lang : 'fr', //OPTIONAL LANGUAGE
		maptype : 'watercolor', //OPTIONAL MAP STYLE
		css : 'css_timeline/timeline.css', //OPTIONAL PATH TO CSS
		js : 'js_timeline/timeline.js' //OPTIONAL PATH TO JS
	}
</script>

<script type="text/javascript" charset="utf-8" language="javascript"
	src="js_timeline/storyjs-embed.js"></script>
