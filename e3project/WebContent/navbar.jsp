
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container" style="width: auto">
			<a class="btn btn-navbar" data-target=".nav-collapse"
				data-toggle="collapse"> <span class="icon-bar"></span> <span
				class="icon-bar"></span> <span class="icon-bar"></span>
			</a> <a class="brand" href="#"><img src="img/logo.jpg"
				id="velos_logo" /></a>
			<div class="nav-collapse">

				<ul class="nav">

					<li></li>
					<li class=""><a href="landing.html"><i class="icon-home"></i>
							Home</a></li>
					<li class="active"><a href="patientlist.html"><i
							class="icon-user"></i> Patient</a></li>
					<li class=""><a href="study.html"><i class="icon-book"></i>
							Study</a></li>
					<li class=""><a href="schedule.html"><i
							class="icon-calendar"></i> Schedule</a></li>

				</ul>
				<ul class="nav pull-right">
					<li class="dropdown"><a id="drop1" class="dropdown-toggle"
						data-toggle="dropdown" role="button" href="#">Welcome, User <b
							class="caret"></b>
					</a>
						<ul class="dropdown-menu" aria-labelled by="drop3" role="menu">
							<li role="presentation"><a href="#" tabindex="-1"
								role="menuitem">Settings</a></li>
							<li role="presentation"><a href="#" tabindex="-1"
								role="menuitem">Logout</a></li>
						</ul></li>
				</ul>
			</div>

		</div>
	</div>
	<!--/.nav-collapse -->

	<div id="banner" align="center">Patient ID: 10753 &nbsp; First
		Name: John &nbsp; Last Name: Smith &nbsp; DOB: Nov 16, 1983 &nbsp;
		Gender: Male &nbsp; Age: 30 &nbsp; Organization: Velos
		
	</div>

<div id="breadcrumb">
	<ul class=" ">
		<!-- <li><a href="landing.html">Home</a> <span class="divider">/</span></li> -->
		<li><a href="patientlist.html">Patient List</a> <span
			class="divider">/</span></li>
		<li class="active"><a href="patient.html">Patient ID: 10753</a></li>
	</ul>
</div>
</div>

