<!-- stylesheets -->
<link rel="shortcut icon" href="img/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/jquery.slidepanel.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
<link rel="stylesheet" type="text/css"
	href="css/bootstrap-responsive.css">
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css"> -->
<link rel="stylesheet" type="text/css" href="css/datepicker.css">
<link rel="stylesheet" type="text/css" href="css/DT_bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/hint.css">
<link rel="stylesheet" type="text/css" href="css/wsw-style.css">
<!-- <link rel="stylesheet" type="text/css" href="css/hint.min.css"> -->
<link rel="stylesheet" type="text/css" href="css/fullcalendar.css">

<!--js files -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.slidepanel.js"></script>

<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ColReorder.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ColVis.js"></script>
<script type="text/javascript" charset="utf-8"
	src="js/ColReorderWithResize.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/bootstrap.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/appPatient.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript"
	src="js/jquery.slimscroll.min.js"></script>

<!-- Velos config -->
<script type="text/javascript" src="js/velos-original-config.js"></script>
<script type="text/javascript" src="js/velos-custom-config.js"></script>
