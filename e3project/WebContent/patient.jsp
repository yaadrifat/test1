<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Velos E3</title>
<jsp:include page="wsw-all-include.jsp" flush="true"></jsp:include>
<script type="text/javascript" charset="utf-8" src="js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ColReorder.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ColVis.js"></script>
<script type="text/javascript" charset="utf-8" src="js/ColReorderWithResize.js"></script>
<!-- <script type="text/javascript" charset="utf-8" src="js/appPatient.js"></script>
 -->
</head>

<body>
	<jsp:include page="navbar.jsp" flush="true"></jsp:include>

	<div class="accordion" id="PatientDemographicsAccordion">
		<jsp:include page="patient_info.jsp" flush="true"></jsp:include>
	</div>

	<div class="accordion" id="pat_studyListAccordion">
		<jsp:include page="patient_studylist.jsp" flush="true"></jsp:include>
	</div>
	<div class="accordion" id="scheduleOfEventsAccordion">
		<jsp:include page="patient_schedulelist.jsp" flush="true"></jsp:include>
	</div>

	<div class="accordion" id="timelineAccordion">
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" data-toggle="collapse"
					data-parent="#timelineAccordion" href="#timelineCollapse">
					Timeline </a>
			</div>
			<div id="timelineCollapse" class="accordion-body collapse in">
				<div class="accordion-inner">
					<div class="container">
						<div id="timeline-embed"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="patientModalInclude">
		<jsp:include page="patient_modal.jsp" flush="true"></jsp:include>
	</div>

</body>
</html>
