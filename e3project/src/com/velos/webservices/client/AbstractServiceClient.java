package com.velos.webservices.client;

import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

public abstract class AbstractServiceClient {
	
	public static final String EMPTY_STR = "";
	public static final String ISSUES_STR = "issues";
	protected String domain;
	protected String userId;
	protected String password; 
	
  	protected void addSecurityInterceptor(String userId, String password, Client client) {
		Endpoint cxfEndpoint = client.getEndpoint();
		Map<String,Object> outProps = new HashMap<String,Object>();
		outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		outProps.put(WSHandlerConstants.PW_CALLBACK_REF, new SimpleCallbackHandler(password));
		outProps.put(WSHandlerConstants.USER, userId);
		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
		cxfEndpoint.getOutInterceptors().add(wssOut);
  	}
  	
  	protected String objectToJson(Object object) {
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		JsonParser jsonParser = new JsonParser();
  		JsonElement jsonElement = null;
  		try {
  			jsonElement = jsonParser.parse(new Gson().toJson(object));
  		} catch(Exception e) {
  			e.printStackTrace();
  			return EMPTY_STR;
  		}
  		return jsonElement.toString();
  	}
  	
  	protected String objectToJson(String name, Object object) {
  		String jsonName = name == null ? EMPTY_STR : name;
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		JsonParser jsonParser = new JsonParser();
  		JsonElement jsonElement = null;
  		try {
  			jsonElement = jsonParser.parse(new Gson().toJson(object));
  		} catch(Exception e) {
  			e.printStackTrace();
  			return EMPTY_STR;
  		}
  		JsonObject jsonObject = new JsonObject();
  		jsonObject.add(jsonName, jsonElement);
  		return jsonObject.toString();
  	}
  	
  	protected void objectToJsonWriter(String name, Object object, Writer writer) {
  		String jsonName = name == null ? EMPTY_STR : name;
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		JsonParser jsonParser = new JsonParser();
  		JsonElement jsonElement = null;
  		try {
  			jsonElement = jsonParser.parse(new Gson().toJson(object));
  		} catch(Exception e) {
  			e.printStackTrace();
  		}
  		JsonObject jsonObject = new JsonObject();
  		jsonObject.add(jsonName, jsonElement);
  		JsonWriter jsonWriter = new JsonWriter(writer);
  		try {
  			new Gson().toJson(jsonObject, jsonWriter);
  		} catch(JsonIOException e) {
  			e.printStackTrace();
  		}
  	}
  	
  	protected void objectToJsonWriterNoKey(Object object, Writer writer) {
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		JsonParser jsonParser = new JsonParser();
  		JsonElement jsonElement = null;
  		try {
  			jsonElement = jsonParser.parse(new Gson().toJson(object));
  		} catch(Exception e) {
  			e.printStackTrace();
  		}
  		JsonWriter jsonWriter = new JsonWriter(writer);
  		try {
  			new Gson().toJson(jsonElement, jsonWriter);
  		} catch(JsonIOException e) {
  			e.printStackTrace();
  		}
  	}

}
