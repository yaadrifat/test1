package com.velos.webservices.client;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.velos.services.PatientDemographicsSEI;
import com.velos.services.PatientDemographicsService;
import com.velos.services.PatientScheduleSEI;
import com.velos.services.PatientScheduleService;
import com.velos.services.StudyService;
import com.velos.services.StudySEI;

public class VelosServiceLocator {
	
	private static final String WSDL_STUDY_SERVICE = "/webservices/studyservice?wsdl";
	private static final String WSDL_PATIENT_DEMOGRAPHICS_SERVICE = "/webservices/patientdemographicsservice?wsdl";
	private static final String WSDL_PATIENT_SCHEDULE_SERVICE = "/webservices/patientscheduleservice?wsdl";

	public static HashMap<String, String> velosStudyServiceMethods = null;
	public static HashMap<String, String> velosPatientDemographicsServiceMethods = null;
	public static HashMap<String, String> velosPatientScheduleServiceMethods = null;
	public static HashMap<String, HashMap<String, String>> velosServiceRegistry = null;
	
	static {
		velosPatientDemographicsServiceMethods = new HashMap<String, String>();
		velosStudyServiceMethods = new HashMap<String, String>();
		velosPatientScheduleServiceMethods = new HashMap<String, String>();
		for (Method method: PatientDemographicsServiceClient.class.getMethods()) {
			if (!Modifier.isPublic(method.getModifiers())) { continue; }
			if ("PatientDemographicsServiceClient".equals(method.getName())) { continue; }
			velosPatientDemographicsServiceMethods.put(method.getName(), method.getName());
		}
		for (Method method: StudyServiceClient.class.getMethods()) {
			if (!Modifier.isPublic(method.getModifiers())) { continue; }
			if ("StudyServiceClient".equals(method.getName())) { continue; }
			velosStudyServiceMethods.put(method.getName(), method.getName());
		}
		for (Method method: PatientScheduleServiceClient.class.getMethods()) {
			if (!Modifier.isPublic(method.getModifiers())) { continue; }
			if ("PatientScheduleServiceClient".equals(method.getName())) { continue; }
			velosPatientScheduleServiceMethods.put(method.getName(), method.getName());
		}
		velosServiceRegistry = new HashMap<String, HashMap<String, String>>();
		velosServiceRegistry.put("studyservice", velosStudyServiceMethods);
		velosServiceRegistry.put("patientdemographicsservice", velosPatientDemographicsServiceMethods);
		velosServiceRegistry.put("patientscheduleservice", velosPatientScheduleServiceMethods);
	}
	
	public static String getRegisteredServiceMethod(String serviceName, String methodName) {
		HashMap<String, String> methodHash = velosServiceRegistry.get(serviceName);
		if (methodHash == null) { return null; }
		return methodHash.get(methodName);
	}
	
	public static StudySEI getStudyService(String domain) throws MalformedURLException {
		return new StudyService(
				new URL(domain + WSDL_STUDY_SERVICE)).getStudyWSPort();
	}
	
	public static PatientDemographicsSEI getPatientDemographicsSerivce(String domain) throws MalformedURLException {
		return new PatientDemographicsService(
				new URL(domain + WSDL_PATIENT_DEMOGRAPHICS_SERVICE)).getPatientDemographicsWSPort();
	}
	
	public static PatientScheduleSEI getPatientScheduleSerivce(String domain) throws MalformedURLException {
		return new PatientScheduleService(
				new URL(domain + WSDL_PATIENT_SCHEDULE_SERVICE)).getPatientScheduleWSPort();
	}
}
