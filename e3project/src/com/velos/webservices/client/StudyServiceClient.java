package com.velos.webservices.client;

import java.io.PrintWriter;

import org.apache.cxf.frontend.ClientProxy;

import com.google.gson.Gson;
import com.velos.services.OperationException;
import com.velos.services.OperationException_Exception;
import com.velos.services.Study;
import com.velos.services.StudySEI;
import com.velos.services.StudyIdentifier;

public class StudyServiceClient extends AbstractServiceClient {
	
	private final static String STUDY_STR = "study";
	
	private StudySEI studySEI = null;
	
	@SuppressWarnings("unused")
	private StudyServiceClient() {}
	
	public StudyServiceClient(String domain, String userId, String password) {
		this.domain = domain; this.userId = userId; this.password = password;
  		try {
  			studySEI = VelosServiceLocator.getStudyService(domain);
  	  		addSecurityInterceptor(userId, password, ClientProxy.getClient(studySEI));
  		} catch(Exception e) {
			e.printStackTrace();
			studySEI = null;
  		}
	}
	
  	public String getStudy(String input) {
  		StudyIdentifier studyIdentifier = new Gson().fromJson(input, StudyIdentifier.class);
  		Study study = null;
  		try {
			study = studySEI.getStudy(studyIdentifier);
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			return objectToJson(ISSUES_STR, operationException.getIssues());
		}
		return objectToJson(STUDY_STR, study);
  	}
  	
  	public void getStudy(String input, PrintWriter writer) {
  		StudyIdentifier studyIdentifier = new Gson().fromJson(input, StudyIdentifier.class);
  		Study study = null;
  		try {
			study = studySEI.getStudy(studyIdentifier);
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
	  		return;
		}
  		objectToJsonWriter(STUDY_STR, study, writer);
  	}

}
