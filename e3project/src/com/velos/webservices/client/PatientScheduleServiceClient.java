package com.velos.webservices.client;

import java.io.PrintWriter;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.cxf.frontend.ClientProxy;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.velos.services.OperationException;
import com.velos.services.OperationException_Exception;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientSchedule;
import com.velos.services.PatientScheduleSEI;
import com.velos.services.StudyIdentifier;

public class PatientScheduleServiceClient extends AbstractServiceClient {
	
	private final static String PATIENT_SCHEDULE = "patientSchedule";
	private final static String PATIENT_IDENTIFIER = "patientIdentifier";
	private final static String STUDY_IDENTIFIER = "studyIdentifier";
	
	private PatientScheduleSEI patientScheduleSEI = null;
	
	@SuppressWarnings("unused")
	private PatientScheduleServiceClient() {}
			
	public PatientScheduleServiceClient(String domain, String userId, String password) {
		this.domain = domain; this.userId = userId; this.password = password;
		try {
			patientScheduleSEI = VelosServiceLocator.getPatientScheduleSerivce(domain);
			addSecurityInterceptor(userId, password, ClientProxy.getClient(patientScheduleSEI));
		} catch(Exception e) {
			e.printStackTrace();
			patientScheduleSEI = null;
  		}
	}
	
	public void getCurrentPatientSchedule(String input, PrintWriter writer) {
		JsonParser parser = new JsonParser();
		JsonElement jsonElement = null;
		try {
			jsonElement = parser.parse(input);
		} catch(Exception e) {
			e.printStackTrace();
		}
		JsonObject jsonObject = jsonElement.getAsJsonObject();
  		PatientIdentifier patientIdentifier = new Gson().fromJson(jsonObject.get(PATIENT_IDENTIFIER), PatientIdentifier.class);
  		StudyIdentifier studyIdentifier = new Gson().fromJson(jsonObject.get(STUDY_IDENTIFIER), StudyIdentifier.class);
  		PatientSchedule patientSchedule = null;
  		try {
  			XMLGregorianCalendar startDate = null;
  			XMLGregorianCalendar endDate = null;
  			patientSchedule = patientScheduleSEI.getCurrentPatientSchedule(patientIdentifier, studyIdentifier, startDate, endDate);
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
			return;
		}
  		objectToJsonWriter(PATIENT_SCHEDULE, patientSchedule, writer);
	}
	
}
