package com.velos.webservices.client;

import java.util.ResourceBundle;

public class VelosServiceBundle {
	private static ResourceBundle velosServiceBundle = null;
	private static ResourceBundle velosServiceCustomBundle = null;
	
	static {
		try {
			velosServiceBundle = 
				ResourceBundle.getBundle("com/velos/webservices/client/VelosService");
		} catch(Exception e) {
			velosServiceBundle = null;
		}
		try {
			velosServiceCustomBundle = 
					ResourceBundle.getBundle("com/velos/webservices/client/VelosServiceCustom");
		} catch(Exception e) {
			velosServiceCustomBundle = null;
		}
	}
	
	private static final String EMPTY_STR = "";
	
	public static String getProperty(String key) {
		String value = null;
		try {
			value = velosServiceBundle.getString(key);
		} catch(Exception e) {
			value = EMPTY_STR;
		}
		if (velosServiceCustomBundle == null) { return value; }
		try {
			String tmp = velosServiceCustomBundle.getString(key);
			if (tmp != null && tmp.length() > 0) {
				value = tmp; 
			}
		} catch(Exception e) {}
		return value;
	}
}
