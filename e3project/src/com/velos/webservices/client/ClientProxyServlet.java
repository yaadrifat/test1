package com.velos.webservices.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ClientProxyServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static String[] allowedHosts = null;
	
	private final static String CLIENT_PROXY = "clientproxy";
	private final static String RESTFUL_PROXY = "restfulproxy";
	private final static String LOCAL_STR = "local";
	private final static String REMOTE_STR = "remote";
	private final static String INPUT_STR = "input";
	private final static String PAGE_404 = "/404.html";
	private final static String PLUS_REGEXP = "\\+";
	private final static String SLASH_STR = "/";
	private final static String AUTHORIZATION_STR = "Authorization";
	private final static String EMPTY_STR = "";
	private final static String SPACE_STR = " ";
	private final static String COLON_STR = ":";
	private final static String ALLOWED_HOST_LIST = "allowed.host.list";
	private final static String KEY_PROXY_TYPE = "proxyType";
	private final static String KEY_LOCAL_OR_REMOTE = "localOrRemote";
	private final static String KEY_DOMAIN = "domain";
	private final static String KEY_SERVICE_NAME = "serviceName";
	private final static String KEY_METHOD_NAME = "methodName";
	private final static String SVC_TYPE_STUDY = "studyservice";
	private final static String SVC_TYPE_PATIENT_DEMOGRAPHICS = "patientdemographicsservice";
	private final static String SVC_TYPE_PATIENT_SCHEDULE = "patientscheduleservice";
	
	private final static int PROXY_TYPE_NONE = 0;
	private final static int PROXY_TYPE_CLIENT = 1;
	private final static int PROXY_TYPE_RESTFUL = 2;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientProxyServlet() {
        super();
        loadAllowedHosts();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		callService(request, response, "GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		callService(request, response, "POST");
	}
	
	
	private void callService(HttpServletRequest request, HttpServletResponse response, String callType)
	throws ServletException, IOException {
		System.out.println(request.getRequestURI());
		HashMap<String, String> parsedPath = null;
		try {
			parsedPath = parseRequestPath(request.getRequestURI());
		} catch (Exception e) {
			request.getRequestDispatcher(PAGE_404).forward(request, response);
			return;
		}
		
		if (RESTFUL_PROXY.equals(parsedPath.get(KEY_PROXY_TYPE))) {
			callRestfulService(request, response, callType);
			return;
		} else if (CLIENT_PROXY.equals(parsedPath.get(KEY_PROXY_TYPE))) {
			callClientService(request, response, parsedPath);
			return;
		} else {
			request.getRequestDispatcher(PAGE_404).forward(request, response);
			return;
		}
	}
	
	private void callRestfulService(HttpServletRequest request, HttpServletResponse response, String callType)
	throws ServletException, IOException {
		response.setContentType("application/json");
		URL url = new URL(request.getParameter("url"));		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(callType);
		conn.setRequestProperty("Content-Type", "application/json");	
	
		conn.setRequestProperty ("Authorization", 	request.getHeader("Authorization"));
		String input = request.getParameter("input");
 
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
 
		PrintWriter writer = response.getWriter();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
//			throw new RuntimeException("Failed : HTTP error code : "
//				+ conn.getResponseCode());
			writer.println("Error Occured" + conn.getResponseCode()); 
			writer.flush();
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
	 
			String output;
			
			while ((output = br.readLine()) != null) {
				writer.println(output);
			}			
			writer.flush();			
		}
	}

	private void callClientService(HttpServletRequest request, HttpServletResponse response,
			HashMap<String, String> parsedPath)
	throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		String authHeader = request.getHeader(AUTHORIZATION_STR);
		if (authHeader == null || authHeader.length() < 1) {
			request.getRequestDispatcher(PAGE_404).forward(request, response);
			return;
		}
		String userId = null;
		String password = null;
		String parts[] = authHeader.split(SPACE_STR);
		for (int iX=0; iX<parts.length; iX++) {
			if (parts[iX] == null || !parts[iX].contains(COLON_STR)) { continue; }
			String subparts[] = parts[iX].split(COLON_STR);
			userId = subparts[0];
			password = subparts[1];
		}
		String domain = null;
		if (REMOTE_STR.equals(parsedPath.get(KEY_LOCAL_OR_REMOTE))) {
			domain = parsedPath.get(KEY_DOMAIN);
		} else {
			domain = LOCAL_STR;
		}
		
		PrintWriter writer = response.getWriter();
		if (SVC_TYPE_STUDY.equals(parsedPath.get(KEY_SERVICE_NAME))) {
			String registeredServiceMethod = VelosServiceLocator.getRegisteredServiceMethod(
					SVC_TYPE_STUDY, parsedPath.get(KEY_METHOD_NAME));
			if (registeredServiceMethod == null) {
				System.out.println("Invalid request:"+SVC_TYPE_STUDY+"/"+parsedPath.get(KEY_METHOD_NAME));
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
			try {
				Method myMethod = StudyServiceClient.class.getMethod(registeredServiceMethod, String.class, PrintWriter.class);
				myMethod.invoke((Object)new StudyServiceClient(domain, userId, password), 
						request.getParameter(INPUT_STR), writer);
			} catch(Exception e) {
				e.printStackTrace();
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
		} else if (SVC_TYPE_PATIENT_DEMOGRAPHICS.equals(parsedPath.get(KEY_SERVICE_NAME))) {
			String registeredServiceMethod = VelosServiceLocator.getRegisteredServiceMethod(
					SVC_TYPE_PATIENT_DEMOGRAPHICS, parsedPath.get(KEY_METHOD_NAME));
			if (registeredServiceMethod == null) {
				System.out.println("Invalid request:"+SVC_TYPE_PATIENT_DEMOGRAPHICS+"/"+parsedPath.get(KEY_METHOD_NAME));
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
			try {
				Method myMethod = PatientDemographicsServiceClient.class.getMethod(
						registeredServiceMethod, String.class, PrintWriter.class);
				myMethod.invoke((Object)new PatientDemographicsServiceClient(domain, userId, password), 
						request.getParameter(INPUT_STR), writer);
			} catch(Exception e) {
				e.printStackTrace();
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
		} else if (SVC_TYPE_PATIENT_SCHEDULE.equals(parsedPath.get(KEY_SERVICE_NAME))) {
			String registeredServiceMethod = VelosServiceLocator.getRegisteredServiceMethod(
					SVC_TYPE_PATIENT_SCHEDULE, parsedPath.get(KEY_METHOD_NAME));
			if (registeredServiceMethod == null) {
				System.out.println("Invalid request:"+SVC_TYPE_PATIENT_SCHEDULE+"/"+parsedPath.get(KEY_METHOD_NAME));
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
			try {
				Method myMethod = PatientScheduleServiceClient.class.getMethod(
						registeredServiceMethod, String.class, PrintWriter.class);
				myMethod.invoke((Object)new PatientScheduleServiceClient(domain, userId, password), 
						request.getParameter(INPUT_STR), writer);
			} catch(Exception e) {
				e.printStackTrace();
				request.getRequestDispatcher(PAGE_404).forward(request, response);
				return;
			}
		} else {
			request.getRequestDispatcher(PAGE_404).forward(request, response);
			return;
		}
		writer.flush();
	}
	
	private HashMap<String, String> parseRequestPath(String requestPath) throws Exception {
		HashMap<String, String> retMap = new HashMap<String, String>();
		if (requestPath == null || requestPath.length() < 1) {
			throw new Exception();
		}
		String[] path = requestPath.split("/");
		
		int proxyType = PROXY_TYPE_NONE;
		for (int iX=0; iX<path.length; iX++) {
			if (CLIENT_PROXY.equals(path[iX])) {
				proxyType = PROXY_TYPE_CLIENT;
				break;
			} else if (RESTFUL_PROXY.equals(path[iX])) {
				proxyType = PROXY_TYPE_RESTFUL;
				break;
			}
		}
		
		try {
			switch(proxyType) {
			case PROXY_TYPE_CLIENT:
				retMap = parseRequestPathForClientProxy(path);
				break;
			case PROXY_TYPE_RESTFUL:
				retMap = parseRequestPathForRestfulProxy(path);
				break;
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		return retMap;
	}
	
	private HashMap<String, String> parseRequestPathForClientProxy(String[] path) throws Exception {
		HashMap<String, String> retMap = null;
		boolean hasRequired = false;
		for (int iX=0; iX<path.length; iX++) {
			if (!CLIENT_PROXY.equals(path[iX]) && !RESTFUL_PROXY.equals(path[iX])) { continue; }
			if (path.length < iX+5) {
				// Need at least 4 paths after clientproxy
				throw new Exception();
			}
			if (!LOCAL_STR.equals(path[iX+1]) && !REMOTE_STR.equals(path[iX+1])) {
				// The path after clientproxy must be local or remote
				throw new Exception();
			}
			retMap = new HashMap<String, String>();
			retMap.put(KEY_PROXY_TYPE, path[iX]);
			retMap.put(KEY_LOCAL_OR_REMOTE, path[iX+1]);
			retMap.put(KEY_DOMAIN, parseDomainPath(path[iX+2]));
			retMap.put(KEY_SERVICE_NAME, path[iX+3]);
			retMap.put(KEY_METHOD_NAME, path[iX+4]);
			hasRequired = true;
			break;
		}
		if (!hasRequired) {
			throw new Exception();
		}
		return retMap;
	}
	
	private String parseDomainPath(String path) throws Exception {
		if (path == null) { return EMPTY_STR; }
		String parts[] = path.split(PLUS_REGEXP);
		if (parts == null || parts.length < 2) { return EMPTY_STR; }
		if (!checkDomain(parts[1])) { throw new Exception("Host not allowed"); }
		StringBuffer domainSB = new StringBuffer();
		domainSB.append(parts[0]).append(COLON_STR).append(SLASH_STR)
		.append(SLASH_STR).append(parts[1]);
		if (parts.length > 2) {
			domainSB.append(COLON_STR).append(parts[2]);
		}
		return domainSB.toString();
	}
	
	private HashMap<String, String> parseRequestPathForRestfulProxy(String[] path) throws Exception {
		HashMap<String, String> retMap = null;
		boolean hasRequired = false;
		for (int iX=0; iX<path.length; iX++) {
			if (!RESTFUL_PROXY.equals(path[iX])) { continue; }
			if (path.length < iX+2) {
				// Need at least 1 path after restfulproxy
				throw new Exception();
			}
			if (!LOCAL_STR.equals(path[iX+1]) && !REMOTE_STR.equals(path[iX+1])) {
				// The path after restfulproxy must be local or remote
				throw new Exception();
			}
			retMap = new HashMap<String, String>();
			retMap.put(KEY_PROXY_TYPE, path[iX]);
			retMap.put(KEY_LOCAL_OR_REMOTE, path[iX+1]);
		}
		if (!hasRequired) {
			throw new Exception();
		}
		return retMap;
	}
	
	// Call this once at instantiation and don't modify allowedHosts[] thereafter
	private void loadAllowedHosts() {
		String allowedHostList = VelosServiceBundle.getProperty(ALLOWED_HOST_LIST);
		allowedHosts = allowedHostList.split(",");
		for (int iX=0; iX<allowedHosts.length; iX++) {
			if (allowedHosts[iX] == null) { continue; }
			allowedHosts[iX] = allowedHosts[iX].trim();
		}
	}
	
	private boolean checkDomain(String domain) {
		if (LOCAL_STR.equals(domain)) { return true; }
		for (int iX=0; iX<allowedHosts.length; iX++) {
			if (domain.equals(allowedHosts[iX])) {
				return true;
			}
		}
		return false;
	}
	
}
