package com.velos.webservices.client;

import java.io.PrintWriter;

import org.apache.cxf.frontend.ClientProxy;

import com.google.gson.Gson;
import com.velos.services.OperationException;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.Patient;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientDemographicsSEI;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientOrganizations;

public class PatientDemographicsServiceClient extends AbstractServiceClient {
	
	private final static String PATIENT_DEMOGRAPHICS = "patientDemographics";
	
	private PatientDemographicsSEI patientDemographicsSEI = null;
	
	@SuppressWarnings("unused")
	private PatientDemographicsServiceClient() {}
	
	public PatientDemographicsServiceClient(String domain, String userId, String password) {
		this.domain = domain; this.userId = userId; this.password = password;
		try {
			patientDemographicsSEI = VelosServiceLocator.getPatientDemographicsSerivce(domain);
			addSecurityInterceptor(userId, password, ClientProxy.getClient(patientDemographicsSEI));
		} catch(Exception e) {
			e.printStackTrace();
			patientDemographicsSEI = null;
  		}
	}
	
	public void getPatientDemographics(String input, PrintWriter writer) {
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		PatientIdentifier patientIdentifier = new Gson().fromJson(input, PatientIdentifier.class);
  		PatientDemographics patientDemographics = null;
  		try {
  			patientDemographics = patientDemographicsSEI.getPatientDemographics(patientIdentifier);
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
			return;
		}
  		objectToJsonWriter(PATIENT_DEMOGRAPHICS, patientDemographics, writer);
	}
	
	public void getPatientDetails(String input, PrintWriter writer) {
		PatientIdentifier patientIdentifier = new Gson().fromJson(input, PatientIdentifier.class);
		Patient patient = null;
  		try {
  			patient = patientDemographicsSEI.getPatientDetails(patientIdentifier);
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
			return;
		}
  		objectToJsonWriterNoKey(patient, writer);
	}
	
	public void getPatientFacilities(String input, PrintWriter writer) {
  		// This method assumes that the Java object is not a generic (i.e. Collection) type
  		PatientIdentifier patientIdentifier = new Gson().fromJson(input, PatientIdentifier.class);
  		PatientOrganizations patientOrganizations = null;
  		try {
  			patientOrganizations = patientDemographicsSEI.getPatientFacilities(patientIdentifier);
		} catch (OperationRolledBackException_Exception e) {
			OperationRolledBackException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
			return;
		} catch (OperationException_Exception e) {
			OperationException operationException = e.getFaultInfo();
			objectToJsonWriter(ISSUES_STR, operationException.getIssues(), writer);
			return;
		}
  		objectToJsonWriterNoKey(patientOrganizations, writer);
	}
	
}
