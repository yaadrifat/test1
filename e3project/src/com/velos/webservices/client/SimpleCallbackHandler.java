/**
 * 
 */
package com.velos.webservices.client;
 
import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**   
 * @author dylan
 * call back handle class handling authentication of user logging in.
 *
 */
public class SimpleCallbackHandler implements CallbackHandler{
	private String password = null;
	public SimpleCallbackHandler(String password) {
		this.password = password;
	}
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (Callback callback : callbacks) {
			if (callback instanceof WSPasswordCallback) {
				((WSPasswordCallback) callback).setPassword(this.password);
			}
		}
	}
}
