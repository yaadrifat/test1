package com.example.helloworld;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class WebServiceClient extends Activity{
	
	private static final String NAMESPACE = "http://66.237.42.80:8080/webservices/";
	private static final String URL = "http://66.237.42.80:8080/webservices/monitorservice?wsdl";
	private static final String SOAP_ACTION = "http://66.237.42.80:8080/webservices/monitorservice";
	private static final String  METHOD = "getHeartBeat";
	
	private Object result = null;
	
	@Override
	public void onCreate(Bundle icicle){
		super.onCreate(icicle);
		TextView tv = new TextView(this);
		setContentView(tv);
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD);
		
		//SOAP Object
		
		request.addProperty("userName", "tbali");
		request.addProperty("password", "velos123");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try{
			androidHttpTransport.call(SOAP_ACTION, envelope);
			System.out.println("Response3::");
			result = envelope.getResponse();
			String[] results = (String[]) result;
			tv.setText(results[0]);
			System.out.println("Response::"+results.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
